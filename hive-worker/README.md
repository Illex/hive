# HIVE Worker

The worker front end repository for HIVE

The worker will connect to a queen in order to find other workers to store files on.
The worker is effectively the front end of our application and handles basic TCP connections
to the queen, file encryption, and user interaction.

In order to get started, download and install node.js Note: Sometimes this installs an old version of npm/node.js, which has to be updated to the latest version to work<br>
  `apt install npm`

After that completes use npm to install Electron <br>
  `npm install -g electron-prebuilt`

Once that is complete, clone the repository and navigate to the hive-worker/Worker directory then run <br>
  `npm start`

If you are having issues starting the application, try running <br>
  `npm audit fix`



