//A collection of functions and classes for handling all network funcitons
//sockets, https, message encoding and decoding etc.
//import the needed dependencies for basic networking

const net = require('net');
const udp = require('dgram');
const ip = require('ip');

//a network controlling class that will send and receve messages for the main controller (index.js)
class networkController{

    /**
     * a constructor for the networker, use this to initialize the the networker
     * with netowrk information
     * @param {string} addr - the internet domain or "location" of a server this worker is trying to connect to
     * @param {string} Port - The port used when trying to connect to another worker
     * @param {number} Id - the unique identifier assigned to this worker on the cluster
     * @param {function} oWriteFunc - A callback to be used when the overwrite command is received: is sent the received JSON object
     * @param {function} registerFunc - A callback to be used when saving the assigned worker id: is sent the received JSON object
     * @param {function} transferFunc - A callback to be used when the client needs to send something back to the server
     * @param {function} fileLocFunc - A callback to be used when the client needs to send something back to the server
     * @param {function} storeFileFunc - A callback to be used when the client needs store a file for another peer
     * @param {function} sendToPeerFunc - A callback to be used when the client needs to connect to a peer to upload a file
     * @param {function} getFileContentsFunc - A callback to be used when the the networker needs to send a file on a connection 
     **/ 
    constructor(addr, port, Id, oWriteFunc, registerFunc, transferFunc, fileLocFunc, storeFileFunc, sendToPeerFunc, getFileContentFunc, ledgerUpdate, getFileContentByIdFunc, decryptAndStoreFunc, connectedFunc, uploadFunc, returnBlockFunc){

        this.webAddr = null;
        this.destPort = null;
        this.localPort = null;
        this.secondPort = null;
        this.workerID = null;
        this.socket = null;
        this.overwriteCallback = null;
        this.registerCallback = null;
        this.transferCallack = null;
        this.fileLocCallback = null;
        this.storeFileCallback = null;
        this.sendToPeerCallback = null;
        this.getFileContentCallback = getFileContentFunc;
        this.fileLedgerUpdate = ledgerUpdate;
        this.getFileContentByIdCallback = getFileContentByIdFunc;
        this.decryptAndStoreFileCallback = decryptAndStoreFunc;
        this.connectedCallback = connectedFunc;
        this.uploadCallback = uploadFunc;
        this.returnBlockCallback = returnBlockFunc;
        this.delimiter = '\n';
        this.buffer = "";

        //ensure that the arguments aren't null and meet the requirements
        if(!sendToPeerFunc){
            throw new Error('sendToPeerFunc must not be null');
        }
        else{
            this.sendToPeerCallback= sendToPeerFunc;
        }

        if(!storeFileFunc){
            throw new Error('storeFileFunc must not be null');
        }
        else{
            this.storeFileCallback = storeFileFunc;
        }

        if(!transferFunc){
            throw new Error('transferFunc must not be null');
        }
        else{
            this.transferCallack = transferFunc;
        }

        if(!fileLocFunc){
            throw new Error('fileLocFunc must not be null');
        }
        else{
            this.fileLocCallback = fileLocFunc;
        }

        if(!registerFunc){
            throw new Error('register function must not be null');
        }
        else{
           this.registerCallback = registerFunc;
        }

        if(!port){
            console.log('no connection port was given');
            this.destPort = '3000'; 
        }
        else{
            console.log('setting destination port to the given connection');
            this.destPort = port; 
        }
    
        //check if the domain string was null
        if(!addr){
            this.webAddr = 'https://localhost';
        }
        else{
            this.webAddr = addr;
        }

        //check if the worker id exists
        if (!Id){
            //do nothing
        }
        else{
            this.workerID = Id;
        }

        if(!oWriteFunc){
            throw new Error('overwrite function must not be null')
        }
        else{
            this.overwriteCallback = oWriteFunc;
        }

        console.log('creating networker, info:');
        console.log(', webAddr: ' + this.webAddr +'destPort: ' + this.destPort + ', localPort:' + this.localPort);
    }
    
    setID(givenId){
        this.workerID = givenId;
    }


    setSecondPort(pt){
        this.secondPort = pt;
    }

    /**
     *initiate first time connection to the queen, this must happen before all other messages are sent
     **/
    connectToQueen(arg){
        console.log("beginning connect to queen");
        let self = this;
        this.socket = net.createConnection({port: this.destPort, host: this.webAddr});

        //set keep alive so the server can always connect, keep alive delay 25 seconds.
        this.socket.setKeepAlive(true, 0);

        this.socket.on('connect', function (){
            console.log('Successfully connected!');
            self.connectedCallback('connected');
            if(arg.type === "rejoin"){
                let msg = {
                    type: 'connect',
                    ID: arg.id
                }
                self.sendMessage(msg);
            }
        })

        //listens for different kinds of messages and calls the appropriate function 
        this.socket.on('data', function (msg){

            //added to utillise the added buffer class below.  see bottom of file for reference details
            self.pushToBuffer(msg);
            let msg2;
            while (!self.isFinished()) {
                msg2 = self.handleData()
                console.log("full message received, printing message")
            }

            //may be bad idea to do simple try catch, this is here to make catch malformed messages and do nothing
            let data;
            try{
                data = JSON.parse(msg2.toString().split('|')[1])
            }
            catch{
                return
            }

            if(data.type ==='ok'){
                console.log('operation successful!');
            }
            else if(data.type ==='fileRequest'){
                    //some other worker is requesting a file stored on this worker, send back the file contents
                console.log("fileRequest, data.fileID: " + data.fileID);
                let msg = {
                    type: 'fileResponse',
                    fileID: data.fileID,
                    content: self.getFileContentByIdCallback(data.fileID)
                }
                self.sendMessage(msg);
            }
            else if (data.type ==='downloadResponse'){
                //another worker is giving us the file we asked for, save it in the right location
                console.log("a download response was given, continuing chain");
                self.returnBlockCallback(data);
            }
            else if (data.type ==='storeFile'){
                //some other worker is trying to store a file here, store it in the right locaion
                let msg = {
                    type: 'fileAccept',
                    fileID: data.fileID,
                }
                self.storeFileCallback(data);
                //let the uploader know that this message was accepted
                self.sendMessage(msg);
            }
            else if (data.type ==='uploadAccept'){
                //some other worker accepted our file upload, update the gui
                self.uploadCallback(data); 
            }
            //the queen is requesting a file for another worker
            //TODO: no longer in use?
            else if(data.type === 'download'){
                //never used?
                console.log("download from server: " + data.fileID);
                self.downloadCallback(data.fileID);
            }
            else if(data.type === 'upload'){
                console.log("upload from server")
                //never used?
                self.uploadCallback(data);
            }
            else if(data.type ==='error'){
                console.log("error from server")
                throw new Error(data.msg);
                return;
            }
            else if(data.type ==='overwrite'){
                //call the given overwrite function
                //currently not in use
                console.log("overwrite from server")
                self.overwriteCallback(data);
            }
            else if(data.type ==='acceptWorker'){
                console.log("accept worker from server")
                self.workerID == data.workerID;
                self.registerCallback(data);
            }
            //queen has told us where the file block is located
            else if(data.type ==='fileLoc'){
                self.fileLocCallback(data);
            }
            else if(data.type === 'cacheLoc'){
                //used when the queen has told this worker where it can store it's files
                //make a new callback function to connect to the peer
                self.sendToPeerCallback(data);
            }
        })
    }

    /**
     * attempt to send a message to the queen on the given connection
     * @param {string} msg - the message to be sent on the connected socket
     *      it is assumed that the message is properly formatted.
     */
    sendMessage(msg){
        if(!this.socket){
            throw new Error('there is no active connection: networkController - sendMessage()');
        }

        //add the message size to the actual message
        this.socket.write(JSON.stringify(msg).length + "|" + JSON.stringify(msg));
    }

    /**
     * used to turn the worker into a "server" and begin listening on the specified listen port
     */ 
    //TODO: add a timeout such that if nothing happens within a given time, the server shuts down
    //TODO: this section is no longer in use, may be used if p2p is reimplemented
    beginListen(pt){
        //error handling
        if(!pt){
            throw new Error('argument pt must not be null');
        }

        let self = this;
        //create a listening server socket on the proper port
        console.log("creating server on local port: " + pt);
        console.log("being listening");
        let server = net.createServer({port: pt}, (s) => {

            s.setEncoding('utf8');

            s.on('connect', function() {
                console.log("peer has connected in beginListen()");
            })

            s.on('end', () => {
                console.log('client disconnected');
                s.destroy();
            })
            s.on('error', function(err) {
                console.log(err);
            })
            //the incoming data handler
            s.on('data', function(d) {
                let data = JSON.parse(d.toString().split('|')[0]);
                console.log("received message on socket");
                console.log(data);

                if(data.type ==='fileRequest'){
                    //use callbackFunction to figure out which information to send back through the peer tunnel
                    let dataMSG = self.transferCallack(data.fileID);
                    //make the message to be sent
                    let msg = {
                        type: 'fileResponse',
                        fileID: data.fileID,
                        content: dataMSG
                    }
                    //send message back through the peer connection
                    console.log("sending message to peer");
                    console.log(JSON.stringify(msg));
                    s.write(JSON.stringify(msg).length + "|" +  JSON.stringify(msg));
                    //destroy the socket after the message is sent
                    s.destroy();
                }
                if(data.type === 'upload'){
                    //simply write the file to the filesystem
                    self.storeFileCallback(data);
                    let msg = {
                        type: 'acceptFile',
                        fileID: data.fileID
                    }
                    console.log('file accepted, sending ack back to peer')
                    console.log(JSON.stringify(msg));
                    s.write(JSON.stringify(msg).length + "|" +  JSON.stringify(msg));
                    s.destroy();
                }
                else{
                    console.log('ERROR, message type not recognized, sending error');
                    let msg = {
                        type: 'message not recognized',
                    }
                    s.write(JSON.stringify(msg).length + "|" +  JSON.stringify(msg));
                    s.destroy();
                }
            })
        });

        //tells the server to start listening for connections
        server.listen({port: pt});
    }



// an set of functions to handle incoming messages, ensures a whole json object is received before we try to parse it
// this code was taken directly from https://blog.irowell.io/blog/use-a-message-buffer-stack-to-handle-data/
// and was authored by Isaac Rowell on July 31, 2019. I have modified somewhat to match the program needs 
    // TODO: may need to create the buffer inside the callback function to ensure there are no race conditions between buffers

    isFinished() {
        if (this.buffer.length === 0 || this.buffer.indexOf(this.delimiter) === -1){
            return true
        }
        return false
    }

    pushToBuffer(data) {
        this.buffer += data
    }

    getMessage() {
        const delimiterIndex = this.buffer.indexOf(this.delimiter)

        if (delimiterIndex !== -1) {
            const message = this.buffer.slice(0, delimiterIndex)
            this.buffer = this.buffer.replace(message + this.delimiter, "")
            return message
        }
        return null
    }

    /**
    * Try to accumulate the buffer with messages
    *
    * If the server isnt sending delimiters for some reason
    * then nothing will ever come back for these requests
    */
    handleData() {
        const message = this.getMessage()
        return message
  }
}
//------------------------------- end referenced code section ---------------------------------

module.exports = {networkController};
