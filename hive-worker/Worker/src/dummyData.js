// A collection of classes and functions for handeling
// all the encryption steps for HIVE.
// Including generating keys, nonces, encrypting and decoding

// Dependencies
var nacl_factory = require('js-nacl');

// Class meant to containerize all of the encryption functionality
// into easily understandable methods 
class encryptionCore{

    // Basic constructor that sets up tha nacl instance
    // in addition to a random nonce TODO: likely will
    // have to set up so nonce is more permananet
    constructor(){
        console.log('in encryption constructor');
        this.nacl = null;
        this._nonce = null;
        nacl_factory.instantiate(function (nacl) {
            console.log(nacl.to_hex(nacl.random_bytes(16)));
          }).then(result => {
            this.nacl = result
            this._nonce = this.nacl.crypto_secretbox_random_nonce();
            // TODO: consider making nonce a more permanant value
          });   
    }
    

    get nonce() {
        return this._nonce;
    }

    set nonce(value) {
        this._nonce = value;
    }

    // Takes in a string key and hashes it to be a byte array as expected
    // for sha256 and NaCl
    getHash(key){
        return this.nacl.crypto_hash_sha256(this.nacl.encode_utf8(key));
    }

    // Encrypts a given string using a given key
    // Note that the correct key format can be recieved by using the getHash() function
    encrypt(message, key){
        return this.nacl.crypto_secretbox(message, this._nonce, key);
    }

    // Decodes an encrypted utf8 byte array using a given key
    // Will only make sense if key and nonce match those used to encrypt
    // Note that the correct key format can be recieved by using the getHash() function
    decode(box, key){
        return this.nacl.crypto_secretbox_open(box, this._nonce, key);
    }
}
module.exports = {encryptionCore};