// A collection of classes and functions for handeling
// all the file managing steps for HIVE.
// Including input file dialogs, storing and retrieving 
// HIVE storage files

// Required dependencies
const { worker } = require('cluster');
const {dialog} = require('electron'); 
var path = require('path');
var fs = require('fs'); 
//let blockSize = 10;
//let blockSize = 500000;
let blockSize = 1000000;

// Class meant to containerize all of the file managing 
// functionality into easily understandable methods 
class fileManager{
    // Presently constructor is empty as that basic
    // functionality does not use any fields.
    // Will likely change later in development
    constructor(){

    }

    // Async function that opens the OS's file explorer
    // dialog and allows for the selection of a file
    // Returns a result that contains a field .filePaths
    // which contains one string representing the file path
    // to the selected file.
    async openDialog(){
        return dialog.showOpenDialog({ 
            properties: ['openFile']
            })
    }

    // Async function that opens the OS's file explorer
    // dialog and allows for the selection of a folder
    // Returns a result that contains a field .filePaths
    // which contains one string representing the file path
    // to the selected folder.
    async openDirectory(){
        return dialog.showOpenDialog({ 
            properties: ['openDirectory']
            })
    }

    // Function that takes in a filepath
    // and returns a string with all of the content
    // within the file
    readFile(filepath){
        return fs.readFileSync(filepath);
    }

    // Function that takes in a filepath for a binary file
    // and returns a Uint8Array representing the file
    readUint8ArrayFile(filepath){
        var array = fs.readFileSync(filepath).toString().split("\n");
        var returnArray = new Uint8Array(parseInt(array[0]));
        for (let i = 1; i < array.length; i++) {
            returnArray[i-1] = parseInt(array[i]);
        }
        return returnArray;
    }

    // Function that takes in a filepath and filecontents
    // and wrties those contents to the filepath encoded
    // in utf-8
    writeUint8ArrayFile(filepath, filecontents){
        var logger = fs.createWriteStream(filepath, {
            flags: 'w' 
          })
        logger.write(filecontents.length.toString())
        for (var entry of filecontents.entries()) {
            logger.write("\n")
            logger.write(entry[1].toString());
        }
        logger.end();
    }
    
    // Function that takes in a string derived from a Uint8Array.toString()
    // and returns a Uint8Array representing the string
    creatUint8ArrayFromString(uint8String){
        var array = uint8String.split(',');
        var returnArray = new Uint8Array(array.length);
        for (let i = 0; i < array.length; i++) {
            returnArray[i] = parseInt(array[i]);
        }
        return returnArray;
    }

    // Function that takes in a filepath and filecontents
    // and wrties those contents to the filepath encoded
    // in binary
    writeFile(filepath, filecontents){
        try {
            if (!this.fileExists(path.dirname(filepath))){
                fs.mkdirSync(path.dirname(filepath));
            }
            //console.log("writing file: fileManager.js") 
            fs.writeFileSync(filepath, filecontents); 
        }
        //cannot alert, there is no web page to do so since this is on the back end
        //catch(e) { alert('Failed to save the file !'); }
        catch(e) { console.log("cannot write to file: " + e) }
    }

    // Function that takes a filepath and splits it apart
    // into chunks in a buffer as specified by "blockSize"
    // Returns said buffer containing split apart chunk
    breakIntoBlocks(filePath){
        try{
            var fileBuffer = fs.readFileSync(filePath);
            var bufferLength = fileBuffer.length;
            var result = this.splitBuffer(fileBuffer);
            return result;
        }
        catch(e) { console.log(e) }
    }


    // Splits up a buffer from a readFileSync call
    // into an array  of blocks based off a spcified block size
    // Returns array containing blocks formed from file
    splitBuffer(buffer) {
        let pointer = 0;
        var result = [];
        var currentSize = 0;
        var blockQuantity = buffer.length / blockSize;
    
        for (let i = 0; i < blockQuantity; i++) {
            result[i] = buffer.slice(pointer, pointer + blockSize);
            currentSize += result[i].length;
            pointer += blockSize;
        }
        return result
    }
    
    // Writes content to a block with a given guid
    // Throws an error if block with guid doesn't exist
    writeBlock(guid, content) {
        var fileName = "./blocks/" + guid;
        try {
            if (fs.existsSync(fileName)) {
                fs.writeFileSync(fileName, content);
            }
            else{
                throw new Error("guid does not exists in this worker");
            }
          } catch(err) {
              console.error(err);
          }
    }

    // Writes content to a block with first uuid and overwrites name with second uuid
    // Throws an error if block with guid doesn't exist
    overwriteBlock(origUuid, newUuid, content) {
        var orgFileName = "./blocks/" + origUuid;
        var newFileName = "./blocks/" + newUuid;
        try {
            if (fs.existsSync(orgFileName)) {
                fs.writeFileSync(orgFileName, content);
                fs.rename(orgFileName, newFileName, function(err) {
                    if ( err ) console.log('ERROR: ' + err);
                });
            }
            else{
                throw new Error("guid " + origUuid + " does not exists in this worker, and thus cannot be overwritten by " + newUuid);
            }
          } catch(err) {
              console.error(err);
          }
    }

    // Reads block of specified guid
    readBlock(guid){
        return fs.readFileSync("./blocks/" + guid);
    }

    // Generates blocks with names specified in input array
    // with random strings of length blockSize
    populateBlocks(blockIds){
        try{
            if (!this.fileExists("./blocks")){
                fs.mkdirSync("./blocks");
            }
            var fileName;
            for (let i = 0; i < blockIds.length; i++) {
                fileName = "./blocks/" + blockIds[i];
                fs.writeFileSync(fileName, this.makeString(blockSize));
            }
        }
        catch(e) { console.log(e) }
    }

    // Generates a random string of a specified length
    makeString(length) {
        var result           = '';
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
          result += characters.charAt(Math.floor(Math.random() * 
     charactersLength));
       }
       return result;
    }

    // Writes the mapping between files and uuids to the metadata
    // Stored in two locations, the loaded metadata and the profile
    // directory corresponding to the loaded metadata
    writeSavedFilesMetadata(filesDict, currentProfile){
        try {
            let dict = JSON.stringify(filesDict, null, 2);
            fs.writeFileSync('./metadata/SavedFiles', dict);
            console.log('./profiles/' + currentProfile + '/SavedFiles.meta');
            console.log(dict);
            fs.writeFileSync('./profiles/' + currentProfile + '/SavedFiles.meta', dict);
        } catch (error) {
            console.error(err);
        }
    }

    // Writes the queen info necessary for reconnection into metadata
    // Stored in two locations, the loaded metadata and the profile
    // directory corresponding to the loaded metadata
    writeQueenSettingsMetadata(currentProfile, workerID, webAddress, port, numOfBlocks, profileName){
        try {
            fs.writeFileSync('./metadata/QueenInfo', workerID + ',' + webAddress + ',' + port + ',' + numOfBlocks + ',' + profileName);
            fs.writeFileSync('./profiles/' + currentProfile + '/QueenInfo.meta', workerID + ',' + webAddress + ',' + port + ',' + numOfBlocks + ',' + profileName);
        } catch (error) {
            console.error(err);
        }
    }

    // Reads the QueenInfo metadata file
    readQueenSettingsMetadata(){
        try {
            return (fs.readFileSync('./metadata/QueenInfo', 'utf-8')).split(',');
        } catch (error) {
            console.error(err);
        }
    }

    // Writes a list containing the profile names into the profiles directory
    // Allows for list to be loaded upon boot
    writeProfileList(list){
        try {
            fs.writeFileSync('./metadata/Profiles', JSON.stringify(list));
        } catch (error) {
            console.error(err);
        }
    }

    // Wrapper method that checks if a path exists
    fileExists(path){
        try {
            if (fs.existsSync(path)) {
              return true;
            }
        } catch(err) {
            console.error(err);
        }
        return false;
    }

    // Takes the currently loaded metadata/profile
    // and copies it into the specified path
    exportMetadata(path){
        try {
            let folderPath = path+"/HIVEMetadata";
            fs.mkdirSync(folderPath);
            fs.copyFile('./metadata/nonce', folderPath+"/nonce.meta", fs.constants.COPYFILE_EXCL, (err) => {});
            fs.copyFile('./metadata/QueenInfo', folderPath+"/QueenInfo.meta", fs.constants.COPYFILE_EXCL, (err) => {});
            fs.copyFile('./metadata/SavedFiles', folderPath+"/SavedFiles.meta", fs.constants.COPYFILE_EXCL, (err) => {});
        } catch(err) {
            console.error(err);
        }
    }

    // Takes a path to a folder containing metadata
    // and loads it as the current metadata
    importMetadata(path){
        try {
            fs.copyFile(path+"/nonce.meta",'./metadata/nonce', fs.constants.COPYFILE_FICLONE, (err) => {});
            fs.copyFile( path+"/QueenInfo.meta", './metadata/QueenInfo',fs.constants.COPYFILE_FICLONE, (err) => {});
            fs.copyFile( path+"/SavedFiles.meta",'./metadata/SavedFiles', fs.constants.COPYFILE_FICLONE, (err) => {});
        } catch(err) {
            console.error(err);
        }
    }

    // Checks to make sure all the metadata filepaths exist
    // if one does not exist, it will be created
    checkAndInitializeFolders(){
        try {
            if (!this.fileExists("./metadata")){
                fs.mkdirSync("./metadata");
            }
            if (!this.fileExists("./blocks")){
                fs.mkdirSync("./blocks");
            }
            if (!this.fileExists("./profiles")){
                fs.mkdirSync("./profiles");
            }
        } catch(err) {
            console.error(err);
        }
    }
}


module.exports = {fileManager};
