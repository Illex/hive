const { app, BrowserWindow, dialog } = require('electron');
const path = require('path');
const ipcMain = require('electron').ipcMain;
const netcore = require('./networkCore.js');
const encryptcore = require('./encryptionCore.js');
const filecore = require('./fileManager.js');
const ip = require('ip');
const { Console, profile } = require('console');

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require('electron-squirrel-startup')) { // eslint-disable-line global-require
    app.quit();
}

const createWindow = () => {
    // Create the browser window.
    const mainWindow = new BrowserWindow({
        width: 1050,
        height: 600,
        //these preferences cause a security risk when running code from unknown sources, since we will only run our
        //own internal code, this is not an issue. this is required to allow the "front end" to communicate with the main
        //node process
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false,
            enableRemoteModule: true
        }
    });
    windowContext = mainWindow;

    // and load the index.html of the app.
    mainWindow.loadFile(path.join(__dirname, 'index.html'));

    // Open the DevTools.
    //mainWindow.webContents.openDevTools();

    //setup the worker
    //webAddress should be loaded by the load function
    loadMetadata();
    console.log("loaded connection info")
    console.log(queenAddress)
    console.log(queenPort)
    try {
        if (queenAddress !== null && queenPort !== null) {
            console.log("attempting to connnect")
            networker = new netcore.networkController(queenAddress, queenPort, 0, overwriteFunc, saveID, transferFunc, fileLocFunc, storeFileFunc, sendToPeerFunc, getFileContents, fileLedgerUpdate, getFileContentsByID, decryptAndStoreFileFunc, connectedFunc, uploadFunc, returnBlockFunc);

            let a = {
                type: 'rejoin',
                id: workerID
            }
            networker.connectToQueen(a);
            networker.setID(workerID);
            //TODO: change this to only work on success
            setTimeout(() => mainWindow.webContents.send('connected', networker.webAddr), 3000);
            setTimeout(() => mainWindow.webContents.send('setID', workerID), 3000);
            setTimeout(() => mainWindow.webContents.send('setprofile', currentProfileName), 3000);
        }
        else {
            setTimeout(() => mainWindow.webContents.send('initialsetup', "")
                , 3000);
            setTimeout(() => openAlert("Welcome!", "Welcome to HIVE! Please begin by connecting to a Queen Server"), 3000);
            console.log("no connection information found in metadata, must register as new worker")
            
        }
    }
    catch {
        console.log("nothing to connect to")
        //do nothing
    }

    //waits for the window to load then sends all the file to the front end for display
    setTimeout(() => mainWindow.webContents.send('rebound', fileLedger), 3000);
    setTimeout(() => mainWindow.webContents.send('setUsage', usedBlocks), 3000);
    if (profileList.length > 0)
        setTimeout(() => mainWindow.webContents.send('profileAdd', profileList), 3000);

    mainWindow.on('close', function (e) {
        if (encKey && preventClose) {
            e.preventDefault();
            var choice = dialog.showMessageBox(mainWindow,
                {
                    type: 'question',
                    buttons: ['Yes', 'No'],
                    title: 'Save Your Password!!!',
                    message: 'Have you saved your encryption key somehwere?\nIf you lose your key, your data may be LOST FOREVER!!!'
                });
            choice.then(function (res) {
                if (res.response == 0) {
                    preventClose = false;
                    mainWindow.close();
                }
            })
        }
    });

};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', () => {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) {
        createWindow();
    }
});


// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.
//~~~~~~~~~~~~~~~~~~~~~~~begin controller code ~~~~~~~~~~~~~~~~~~~~~~~~~~

//****event handlers for front end messages to this controller****
var networker;
var workerID;
var filePath;
var fileContents;
var uuid;
var fileLedger = new Array();
var donation;
var encKey;
var savedFiles = {};
var windowContext;
var queenAddress = null;
var queenPort = null;
var downloadPath;
var currentProfileName;
var profileList = new Array();
var preventClose = true;
var oddFlag = false;

var usedBlocks = {
    used: 0,
    total: 0
};

//a global location for a file's blocks to be saved during the download/upload process
//figure out the uuid to block mapping of split buffers
var blocks = [];
var blockCounter = 0;
var blockLedger = [];

const naclCore = new encryptcore.encryptionCore();

//event handler that will listen for a request to make the networker object
ipcMain.on('connect', (event, arg) => {
    console.log("received message from front");
    console.log(arg);
    queenAddress = arg['webAddr'];
    queenPort = arg['port'];
    networker = new netcore.networkController(arg['webAddr'], arg['port'], 0, overwriteFunc, saveID, transferFunc, fileLocFunc, storeFileFunc, sendToPeerFunc, getFileContents, fileLedgerUpdate, getFileContentsByID, decryptAndStoreFileFunc, connectedFunc, uploadFunc, returnBlockFunc);
    networker.connectToQueen('');

    //temporary section to facilitate gui loading
})

//event handler to try and connect to another worker
ipcMain.on('tryConnect', (event, arg) => {
    //console.log("received try connect from front");
    //console.log(arg);
})

//event handler to try and send a message to connected worker
//this worker is telling the queen it wants to store some file on the cluster
//
//TODO: I don't think this is in use anymore
//ipcMain.on('upload', (event, arg) => {

//   console.log('uploading file');
//TODO: change this to get a uuid from the queen instead of making one
//  this.uuid = makeUuid();

//send this to queen to find a peer too store file
// let msg = {
//    type: 'upload',
//   fileID: this.uuid,
//}

//networker.sendMessage(msg)
//})

//function that will allow the user to export their metadata to a file
ipcMain.on('export', (event) => {
    let filer = new filecore.fileManager();

    // Opens file dialog
    filer.openDirectory()
        .then(result => {
            filer.exportMetadata(result.filePaths[0])
            openAlert('Success', "Metadata successfully exported");
            //windowContext.webContents.send('alertUser', "Metadata successfully exported");
        })
})

//function that will allow the user to export their metadata to a file
ipcMain.on('import', (event) => {
    let filer = new filecore.fileManager();

    // Opens file dialog
    filer.openDirectory()
        .then(result => {
            filer.importMetadata(result.filePaths[0])
            openAlert('Success', "Metadata successfully imported, please restart program to see changes");
            //windowContext.webContents.send('alertUser', "Metadata successfully imported, please restart program to see changes");
        })
})

//connect the queen and ask for a worker id, sends a donation value, and list of dummy uuids
ipcMain.on('register', (event, arg) => {
    if (profileList.includes(arg[1])) {
        openAlert('Attention!', "Duplicate profile names are not allowed");
        //windowContext.webContents.send('alertUser', "Duplicate profile names are not allowed");
        return;
    }

    //turn the argument into a json object
    console.log("received register from front: " + arg[0]);
    console.log("donation is");
    console.log(arg[0]);

    profileList.push(arg[1])
    windowContext.webContents.send('profileAdd', profileList);

    donation = arg[0];
    currentProfileName = arg[1];
    usedBlocks.total = donation;
    let filer = new filecore.fileManager();

    let garbage = [];
    for (let i = 0; i < arg[0]; i++) {
        garbage.push(makeUuid());
    }

    //tell the file manager to generate a bunch of garbage data for each of the given uuids
    filer.populateBlocks(garbage);

    //also save the nonce to the metadata so files can be decrypted between sessions
    filer.writeFile("./metadata/nonce", naclCore._nonce);
    filer.writeFile("./profiles/" + currentProfileName + "/nonce.meta", naclCore._nonce);
    filer.writeProfileList(profileList);

    //send the list of dummy uuids along with the register request
    let msg = {
        type: 'join',
        donation: donation,
        uuids: garbage
    }

    usedBlocks.total = donation;

    networker.sendMessage(msg);
})

//send download request to the queen which sets off a chain reaction to connect to another worker 
//for the file.
ipcMain.on('download', (event, arg) => {
    //recieved an array containig a single filename,  get all the uuid's of that filename and begin downloading them
    //do this in a chained fashion
    //if there are no mor uuid's in the file list, break the download cycle
    console.log("received download from front: " + arg);
    fileName = arg[0];
    //reassign the key to whatever the user entered
    encKey = naclCore.getHash(arg[1]);

    let filer = new filecore.fileManager();

    // Opens file dialog
    filer.openDirectory()
        .then(result => {
            downloadPath = result.filePaths[0]

            //arg is an array with 1 element

            //get all the block id's for the associated file
            blockLedger = savedFiles[fileName];
            //begin the file download process

            let msg = {
                type: 'download',
                fileID: blockLedger[blockCounter]
            }
            networker.sendMessage(msg);

            //setup the gui progress bar
            let info = {
                type: "setup",
                max: blockLedger.length
            }
            windowContext.webContents.send('setProgress', info);
        })
})

//event that is called when the user tries to upload a file. 
//first assignes the user supplied key, then opens the file dialogue to select the file
//then splits the file into all it's parts
ipcMain.on('selectFile', (event, arg) => {

    //set the encryption key for this file
    encKey = naclCore.getHash(arg);
    //make a new file management object
    let filer = new filecore.fileManager();

    // Opens file dialog
    filer.openDialog()
        .then(result => {
            filePath = result.filePaths[0]
            fileName = path.basename(filePath);
            fileContents = filer.readFile(result.filePaths[0])
            console.log("file path ***************")
            console.log(filePath)
            console.log("file contents ***************")
            console.log(fileContents)
            //maybe put everything in here to force it to wait for the file explorer to close?


            //if the filename is already in the savedFiles list, send an alert to
            //the user and simply return
            if (fileName in savedFiles) {
                openAlert('Attention!', "Duplicate filenames not allowed, please rename the file you wish to upload")
                //windowContext.webContents.send('alertUser', "Duplicate filenames not allowed, please rename the file you wish to upload");
                return;
            }

            //getting the file encrypted and getting the file back as an array buffer
            let buffer = fileContents;
            console.log('the buffer, will be encrypted later; ' + buffer)

            //now an array of buffer arrays?
            blocks = filer.splitBuffer(Buffer.from(encDriver(buffer)));

            //setup the gui progress bar
            let info = {
                type: "setup",
                max: blocks.length
            }

            //check to make sure the user has enough space on the netork to upload before actually upoloading
            console.log("checking for enough space")
            console.log(blocks.length)
            console.log(usedBlocks.total)
            console.log(usedBlocks.used)
            if (blocks.length > (usedBlocks.total / 1.333) - usedBlocks.used) {
                //send alert
                openAlert("Error", "Not enough storage space left, please delete a file to make room");
                //windowContext.webContents.send('alertUser', "Not enough storage space left, please delete a file to make room");
                //just unlocks the buttons
                windowContext.webContents.send('rebound', fileLedger);
                return;
            }
            //else update the used blocks information
            else {
                usedBlocks.used += blocks.length;
                windowContext.webContents.send('setUsage', usedBlocks);
            }


            windowContext.webContents.send('setProgress', info);

            //call the upload function, upload automatically tries to upload on the established connection, if there is none. it will show this to the user
            uploadFunc(0)
        })
})

ipcMain.on('deleteFile', (event, arg) => {
    //get all the uuid's for the given saved file

    console.log('deleteFile');
    console.log(arg);

    //send an array of uuid's to the queen in the delete message
    let fileList = savedFiles[arg];
    //fileLedger.(fileName);


    //remove name from ledger
    let index = fileLedger.indexOf(arg)

    if (index > -1) {
        fileLedger.splice(index, 1);
    }


    //send the list of uuid's for that file to the queen
    let msg = {
        type: 'delete',
        uuids: fileList
    }
    //update the used blocks
    usedBlocks.used -= savedFiles[arg].length;
    windowContext.webContents.send('setUsage', usedBlocks);

    //remove that file from our own metadata storage and our own saved files datastructure
    delete savedFiles[arg];
    let filer = new filecore.fileManager();
    filer.writeSavedFilesMetadata(savedFiles, currentProfileName);
    //update the gui with the deleted file list
    windowContext.webContents.send('rebound', fileLedger);

    //finally, send the delete message to the queen
    networker.sendMessage(msg)

})

ipcMain.on('profileForm', function (event, data) {
    if (data != "no%profiles%exist") {
        try {
            let filer = new filecore.fileManager();
            currentProfileName = data;
            console.log('./profiles/' + data);
            filer.importMetadata('./profiles/' + data);
            loadMetadata();

            if (queenAddress !== null && queenPort !== null) {
                console.log("attempting to connnect")
                networker = new netcore.networkController(queenAddress, queenPort, 0, overwriteFunc, saveID, transferFunc, fileLocFunc, storeFileFunc, sendToPeerFunc, getFileContents, fileLedgerUpdate, getFileContentsByID, decryptAndStoreFileFunc, connectedFunc, uploadFunc, returnBlockFunc);
                networker.connectToQueen('');
                let a = {
                    type: 'rejoin',
                    id: workerID
                }
                networker.connectToQueen(a);
                networker.setID(workerID);
                //TODO: change this to only work on success
                setTimeout(() => windowContext.webContents.send('connected', networker.webAddr), 3000);
                setTimeout(() => windowContext.webContents.send('setID', workerID), 3000);
                setTimeout(() => windowContext.webContents.send('setprofile', currentProfileName), 3000);
            }
            else {
                console.log("error in trying to switch profiles")
            }
        }
        catch {
            console.log("nothing to connect to")
            //do nothing
        }
    }
    setTimeout(() => windowContext.webContents.send('rebound', fileLedger), 3000);
    setTimeout(() => windowContext.webContents.send('setUsage', usedBlocks), 3000);
});

//***********callback functions for different events in the networker***********
//

//this function starts the upload chain for all the blocks in the blocks array
//if the counter == the number of blocks in the buffer, end the chain, else go to the next array index and send it
//call this funciton again on file accept
function uploadFunc(stuff) {

    if (stuff === 0) {
        //don't add the uuid to the list
    }
    //push the uuid to the block ledger?
    else {
        blockLedger.push(stuff.uuid);
    }

    console.log("in uploadFunc loop: blockCounter: " + blockCounter + ", blocks length: " + blocks.length)
    //here we can check against blocks.length because we want to stop after we've uploaded the last block
    //upload accept is the message that calls this function
    //first, put the uuid in the block ledger
    if (blockCounter === blocks.length) {
        //reset the counter for the next upload
        //write filename and block uuid's to metadata
        console.log("upload complete")
        console.log(fileLedger)
        fileLedger.push(fileName);
        console.log("sending rebound")
        windowContext.webContents.send('rebound', fileLedger);

        console.log("in upload func appending to saved files")
        console.log(blockLedger);
        console.log(fileName)

        savedFiles[fileName] = blockLedger;
        //TODO: add check for flag, and append a ? to last uuid
        console.log(savedFiles)
        let filer = new filecore.fileManager();
        filer.writeSavedFilesMetadata(savedFiles, currentProfileName);
        //update the gui to unlock upload/download button and add the uploaded file to the files list
        blockCounter = 0;
        blocks = [];
        blockLedger = []

        let info = {
            type: 'finished'
        }
        windowContext.webContents.send('setProgress', info);

        return;
        //Do nothing else, the upload is finished
    }
    else {
        //make the upload message
        console.log('uploading next block');
        sendToPeerFunc()
    }

    //update the guit to let user know the upload is progressing
    let info = {
        type: "increment",
    }
    windowContext.webContents.send('setProgress', info);

}

// the inverse of the upload function this is assigned to returnBlock in the network controller?
// called when a block is received on the network
function returnBlockFunc(data) {
    //console.log("blocks acquired: " + (blockCounter + 1)+ "/" + savedFiles[fileName].length)
    let filer = new filecore.fileManager();


    //the magic -1 is so that we don't go over the array size when asking for the next block
    //this is an off by 1 fix since the array is 0 indexed
    if (blockCounter === savedFiles[fileName].length - 1) {
        //console.log(data.contents.data);
        console.log("all blocks acquired, reconstructing and writing file")
        windowContext.webContents.send('rebound', fileLedger);
        //blockCounter++;
        //TODO: check for odd flab and remove padded bytes
        blocks[blockCounter] = data.contents.data
        console.log("full list of blocks")
        console.log(blocks)

        //loop through the array and make a buffer
        let tempBuffer = new Uint8Array(((savedFiles[fileName].length - 1) * blocks[0].length) + blocks[blocks.length - 1].length)
        for (i = 0; i < blocks.length; i++) {
            for (j = 0; j < blocks[i].length; j++) {
                tempBuffer[(i * blocks[0].length) + j] = blocks[i][j];
            }
        }

        //console.log("the constructed buffer")
        //console.log(tempBuffer)
        //console.log(downloadPath)
        //console.log(fileName)
        //console.log(decDriver(tempBuffer));
        filer.writeFile(downloadPath + "/" + fileName, decDriver(tempBuffer));

        //reconstruct the blocks, then decrypt them, then save them
        blockCounter = 0;
        blocks = [];
        blockLedger = []

        let info = {
            type: 'finished'
        }
        windowContext.webContents.send('setProgress', info);
        //Do nothing else, the download is finished
        return;
    }
    else {
        //save the block and begin the chain for the next block
        blocks[blockCounter] = data.contents.data
        blockCounter++;

        let msg = {
            type: 'download',
            fileID: blockLedger[blockCounter]
        }
        //sends another block request and which starts the chain over again. repeats until finished
        networker.sendMessage(msg);

        let info = {
            type: 'increment'
        }
        windowContext.webContents.send('setProgress', info);
    }
}


//will be called when the overwrite message has been received from queen
function overwriteFunc(data) {
    //TODO: implement this
    console.log('Not implemented overwrite');
}

//called when the worker is acting as a server peer to another worker
//data is the file id we are looking for
function transferFunc(fileId) {
    console.log("transferFunc, finding file with id:" + fileId);
    let filer = new filecore.fileManager();
    //fileId should simply be a file uuid at this point
    //let tempContents = filer.readFile(fileId);
    let tempContents = filer.readFile(fileId);
    //give the file's contents back to the network module to be sent to the peer
    //console.log("transfer file contents")
    //console.log(tempContents);
    return tempContents;
}

//helper to read a file from the filesystem and encrypt it for travel
function getFileContents() {
    let filer = new filecore.fileManager();
    //update this to dynamically find file using file id and filer
    //let tempContents = filer.readFile(fileId);
    let tempContents = filer.readFile(filePath);
    //console.log("getting file contents")
    //console.log(tempContents);
    return tempContents;
}

//helper to read file contents from the system and send it for storage
function getFileContentsByID(id) {
    let filer = new filecore.fileManager();
    //update this to dynamically find file using file id and filer
    //let tempContents = filer.readFile(fileId);
    let tempContents = filer.readFile("./blocks/" + id);
    //console.log("getting file contents")
    //console.log(tempContents);
    return (tempContents);
    //return tempContents;

}

function encDriver(data) {
    let encryptedData = naclCore.encrypt(data, encKey);
    console.log("encrypted data representation *************************")
    console.log(encryptedData);
    return encryptedData;
}

function decDriver(data) {
    console.log("decrypted data representation *************************")
    console.log(data);
    console.log(encKey);
    let plaintext = naclCore.decode(data, encKey);
    console.log(plaintext);
    return plaintext;
}

//simple callback helper to save a file during upload
//TODO: update this to specifically overwrite a uuid and block with the given one
function storeFileFunc(data) {
    //console.log("storing file storeFileFunc*****************");
    //console.log(data.content)
    //console.log(data)
    let filer = new filecore.fileManager();
    //filer.writeFile(data.fileID, Buffer.from(data.content))

    //TODO fix arguments, should be (oldUuid, newUuid, content)
    filer.overwriteBlock(data.overwriteID, data.fileID, Buffer.from(data.contents));

    return;
}

function decryptAndStoreFileFunc(data) {
    //console.log("decrypting and storing file storeFileFunc*****************");
    //console.log(data.content)
    let filer = new filecore.fileManager();
    filer.writeFile(data.fileID, decDriver(data.content.data));
    return;
}


//called when this worker is given the location of another worker that holds a block
function fileLocFunc(data) {

    //console.log("calling fileLocFunc")

    //after connection, ask the peer for the file
    let msg = {
        type: 'forward',
        srcID: this.workerID,
        dstID: data.peerID,
        action: 'downloadRequest',
        fileID: data.fileID
    }

    //send message to queen requesting the file
    networker.sendMessage(msg);
}

//callback to upload a file to another peer
//function sendToPeerFunc(data){
function sendToPeerFunc() {

    //let filer = new filecore.fileManager();

    //read the file contents then encrypt them to be stored on the network
    //let tempContents = Array.from(encDriver(filer.readFile(filePath)));
    //temp contents should be assigned to whatever block contents are created in original buffer splitting
    let tempContents = blocks[blockCounter];

    //console.log('send to peer, fileID: ' + data.fileID);
    //console.log('the block contents');
    //console.log(blocks);
    //after connection, ask the peer for the file
    let msg = {
        type: 'upload',
        content: tempContents
    }
    //console.log(msg);
    //send message to queen requesting the file
    networker.sendMessage(msg);
    blockCounter++;
}

//will be called when the accept worker message has been received from queen
function saveID(data) {

    //console.log("saving worker id: " + data.workerID);
    workerID = data.workerID;
    let filer = new filecore.fileManager();
    filer.writeQueenSettingsMetadata(currentProfileName, workerID, queenAddress, queenPort, usedBlocks.total, currentProfileName);
    networker.setID(workerID);
    //save the network information to metadata as well
    queenAddress = networker.webAddr;
    //may need to change where the destPort is coming from
    queenPort = networker.destPort;
    windowContext.webContents.send('setID', workerID);
    windowContext.webContents.send('setprofile', currentProfileName);
}

function fileLedgerUpdate(data) {
    fileLedger.push(path.basename(filePath));
    windowContext.webContents.send('rebound', fileLedger);
}

//used by the controller to initialize the worker using the metadata file
function loadMetadata() {
    let filer = new filecore.fileManager();
    filer.checkAndInitializeFolders();
    if (filer.fileExists("./metadata/SavedFiles")) {
        savedFiles = JSON.parse(filer.readFile("./metadata/SavedFiles"));
        fileLedger = new Array();
        fileLedger = fileLedger.concat(Object.keys(savedFiles));
        console.log("Files Ledger:" + fileLedger);

        console.log("loading used blocks")
        //console.log(savedFiles.length)
        console.log(Object.keys(savedFiles).length)
        //loop through the saved files and count all the uuid's to deterine the number of locks used
        let keys = Object.keys(savedFiles);
        for (let i = 0; i < keys.length; i++) {
            console.log(savedFiles[keys[i]])
            usedBlocks.used += savedFiles[keys[i]].length;
            console.log(usedBlocks.used);
        }
    }
    if (filer.fileExists("./metadata/nonce")) {
        setTimeout(function () {
            naclCore.nonce = new Uint8Array(filer.readFile("./metadata/nonce"));
        }, 1000);
    }
    if (filer.fileExists("./metadata/QueenInfo")) {
        let queenInfo = filer.readQueenSettingsMetadata();
        workerID = queenInfo[0];
        queenAddress = queenInfo[1];
        queenPort = parseInt(queenInfo[2]);
        usedBlocks.total = parseInt(queenInfo[3]);
        currentProfileName = queenInfo[4];
    }
    if (filer.fileExists("./metadata/Profiles"))
        profileList = Array.from(JSON.parse(filer.readFile("./metadata/Profiles")));

    console.log("loading connection info")
    console.log(queenAddress)
    console.log(queenPort)
}

//simple callback to update the user gui
function connectedFunc(msg) {
    console.log("connected callback")
    console.log(networker.webAddr);
    //windowContext.webContents.send('conected', this.queenAddress);
    windowContext.webContents.send('connected', networker.webAddr);
}

// Simple function that works like normal javascript alert
// Used due to alert not being thread safe for Windows
function openAlert(title, message) {
    dialog.showMessageBox(windowContext,
        {
            type: 'question',
            buttons: ['Okay'],
            title: title,
            message: message
        });
}

// Handler so that the view can pass an alert to the index
// in order to be handled in a thread safe manner
ipcMain.on('openAlert', function (event, data) {
    openAlert(data[0], data[1]);
});

//taken from online source for temporary uuid creation, will be removed later
//TODO: remove from final code
function makeUuid() {
    var uuidValue = "", k, randomValue;
    for (k = 0; k < 32; k++) {
        randomValue = Math.random() * 16 | 0;

        if (k == 8 || k == 12 || k == 16 || k == 20) {
            uuidValue += "-"
        }
        uuidValue += (k == 12 ? 4 : (k == 16 ? (randomValue & 3 | 8) : randomValue)).toString(16);
    }
    console.log("generated uuid")
    console.log(uuidValue)
    return uuidValue;
}  
