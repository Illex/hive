const electron = require('electron');
const { dialog } = require('electron');
const remote = electron.remote;
const ipc = electron.ipcRenderer;

var connectBtn = document.getElementById('connect-btn');
var uploadBtn = document.getElementById('upload-btn');
var registerBtn = document.getElementById('register-btn');
//temporary download button for the demo
var downloadBtn = document.getElementById('download1');
var deleteBtn = document.getElementById('delete1');
//var clearBtn = document.getElementById('Clear-btn');
//var saveBtn = document.getElementById('save-settingsBtn');
var exportBtn = document.getElementById('export-btn');
var importBtn = document.getElementById('import-btn');

// NEW UI STUFF
var filesBtn = document.getElementById('files-btn');
var queenBtn = document.getElementById('queen-btn');
var profilesBtn = document.getElementById('profiles-btn');
var infoBtn = document.getElementById('info-btn');

var profileChange = document.getElementById('profile-switch');

var idCounter = 1;

var fileLedger;

exportBtn.addEventListener('click', function (event) {
    ipc.send('export');
})

importBtn.addEventListener('click', function (event) {
    ipc.send('import');
})

// Sends alert to the view from index
ipc.on('alertUser', (event, arg) => {
    alert(arg);
})

ipc.on('setUsage', (event, arg) => {
    console.log("setUsage");
    console.log(arg);
    document.getElementById('usage-bar').value = arg.used;
    document.getElementById('usage-bar').max = arg.total / 1.333;
    //document.getElementById('usage-bar').max = arg.total;
})

/* Following 4 methods correspond to the 4 pages of the UI */
filesBtn.addEventListener('click', function (event) {
    document.getElementById("files-btn").classList.add("active");
    document.getElementById("queen-btn").classList.remove("active");
    document.getElementById("profiles-btn").classList.remove("active");
    document.getElementById("info-btn").classList.remove("active");
    document.getElementById("files-section").hidden = false;
    document.getElementById("queen-section").hidden = true;
    document.getElementById("profiles-section").hidden = true;
    document.getElementById("info-section").hidden = true;
    //hide everything
    //show the files page
})
queenBtn.addEventListener('click', function (event) {
    document.getElementById("files-btn").classList.remove("active");
    document.getElementById("queen-btn").classList.add("active");
    document.getElementById("profiles-btn").classList.remove("active");
    document.getElementById("info-btn").classList.remove("active");
    document.getElementById("files-section").hidden = true;
    document.getElementById("queen-section").hidden = false;
    document.getElementById("profiles-section").hidden = true;
    document.getElementById("info-section").hidden = true;
    //hide everything
    //show the queen page
})

profilesBtn.addEventListener('click', function (event) {
    document.getElementById("files-btn").classList.remove("active");
    document.getElementById("queen-btn").classList.remove("active");
    document.getElementById("profiles-btn").classList.add("active");
    document.getElementById("info-btn").classList.remove("active");
    document.getElementById("files-section").hidden = true;
    document.getElementById("queen-section").hidden = true;
    document.getElementById("profiles-section").hidden = false;
    document.getElementById("info-section").hidden = true;
    //hide everything
    //show the profiles page
})

infoBtn.addEventListener('click', function (event) {
    document.getElementById("files-btn").classList.remove("active");
    document.getElementById("queen-btn").classList.remove("active");
    document.getElementById("profiles-btn").classList.remove("active");
    document.getElementById("info-btn").classList.add("active");
    document.getElementById("files-section").hidden = true;
    document.getElementById("queen-section").hidden = true;
    document.getElementById("profiles-section").hidden = true;
    document.getElementById("info-section").hidden = false;
    //hide everything
    //show the info page
})


//saveBtn.addEventListener('click', function(event) {
//   //TODO: implement this to send all relevant data to the controller
//    ipc.send('saveChanges', 'some changes');
//    console.log("Save Changes: implement this");
//})

//helper to highlight and de-highlight the clicked row for selection
function highlight() {

    //clear all highlights
    let tableBody = document.getElementById('file-table-body');
    for (i = 0; i < tableBody.rows.length; i++) {
        tableBody.rows[i].style = "border: 1px #c4c3c3; color:#c4c3c3 ";
        tableBody.rows[i].styled = false;
    }

    this.style = "border: 1px #fccf14; color:#c4c3c3;";
    this.styled = true;
}


//sends a message back to the controller to initate a file delete event
//TODO:make sure to delete proper file
deleteBtn.addEventListener('click', function (event) {
    //get the file id for this specific delete button
    //send a message back to main
    console.log("sending delete file message");
    //get the file name to be deleted

    let fileList = [];

    let tableBody = document.getElementById('file-table-body');
    for (i = 0; i < tableBody.rows.length; i++) {
        if (tableBody.rows[i].styled === true) {
            console.log(tableBody.rows[i].id);
            fileList.push(tableBody.rows[i].id);
        }
    }

    ipc.send('deleteFile', fileList[0]);

})


//not sure what this is for, likely is no longer needed because of function above
ipc.on('filePath', (event, arg) => {
    console.log('received filePath from back')
    document.getElementById('file-label').text = arg;
})

ipc.on('setID', (event, arg) => {
    document.getElementById('worker-Id-label').innerHTML = "My ID: " + arg;
})

ipc.on('setProgress', (event, arg) => {

    if (arg.type === "setup") {
        document.getElementById('progress-message').hidden = true;
        document.getElementById('progress-bar').hidden = false;
        document.getElementById('progress-bar').max = arg.max;
        document.getElementById('progress-bar').value = 0;
    }
    //push the progress bar forward
    else if (arg.type === "increment") {
        document.getElementById('progress-bar').value = document.getElementById('progress-bar').value + 1;
    }
    //display completion message
    else {
        document.getElementById('progress-bar').hidden = true;
        document.getElementById('progress-message').hidden = false;
    }
})

//used from geeks for geeks tutorial
// For Synchronous Message Transfer we are using the 'ipc.sendSync' method
// We do not need to Implement any Callbacks to handle the Response
// The 'ipc.sendSync' method will return the data from the Main Process
//
connectBtn.addEventListener('click', function (event) {

    let info = {
        webAddr: document.getElementById('addr-box').value,
        port: document.getElementById('port-box').value,
        key: document.getElementById('key-box').value
    }

    document.getElementById('connection-label').innerHTML = "Connecting.....";

    console.log("sending init request with info, port: " + info.port + ", host: " + info.webAddr);
    ipc.send('connect', info);
    return;

});

ipc.on('connected', (event, arg) => {
    //update the connection label for the user
    console.log("connected received from back")
    console.log(arg);
    document.getElementById('connection-label').innerHTML = "Connected to: " + arg;

})

//part of the proof of concept above
ipc.on('rebound', (event, arg) => {
    console.log("rebound called building table")
    //document.getElementById('mirrorB-box').value = arg;
    document.getElementById('download1').disabled = false;
    document.getElementById('upload-btn').disabled = false;

    //temporary section to initialize the workers known uploaded files
    fileLedger = arg;
    let tableBody = document.getElementById('file-table-body');
    //clear the table
    tableBody.innerHTML = "";

    for (let i = 0; i < fileLedger.length; i++) {

        let row = tableBody.insertRow(0);
        let first = row.insertCell(0);
        let second = row.insertCell(1);
        let third = row.insertCell(2);

        row.id = fileLedger[i];

        first.innerHTML = fileLedger[i];

        row.onclick = highlight;
        row.style = "background-color: #232323; color:#c4c3c3;";
        row.styled = false;
    }
})

//try to download an existing file from the network
downloadBtn.addEventListener('click', function (event) {
    //TODO: add key popup requirement

    let fileList = [];

    let tableBody = document.getElementById('file-table-body');
    for (i = 0; i < tableBody.rows.length; i++) {
        if (tableBody.rows[i].styled === true) {
            console.log(tableBody.rows[i].id);
            fileList.push(tableBody.rows[i].id);
        }
    }

    if (fileList.length > 0) {
        document.getElementById('download1').disabled = true;
        document.getElementById('upload-btn').disabled = true;
        fileList.push(document.getElementById('key-box').value)
        ipc.send('download', fileList);
        console.log("sending download request for the following files: " + fileList);
    }
    else {
        ipc.send("openAlert", ["Attention!", "Please select a file then click download"]);
        //alert("please select a file then click download.")
    }
    return;
})

//try to register with the queen
registerBtn.addEventListener('click', function (event) {

    //TODO: add logic to calculat the donation amount based on gb chosen
    let mb = document.getElementById('donation-box').value;
    let profileName = document.getElementById('profile-name').value;

    //let mb = Math.floor(GB * 1000)

    //let donation = Math.floor(mb * 1000)


    console.log("sending register request");
    //ipc.send('register', donation);
    ipc.send('register', [mb, profileName]);
    return;

});


//try to upload a file from the local machine onto the network
uploadBtn.addEventListener('click', function (event) {

    //TODO: add key pop up requirement
    let key = document.getElementById('key-box').value;
    //disable upload to force only one upload at a time

    if (key === null || key.length < 7) {
        ipc.send("openAlert", ["Attention!", "Please enter an encryption key longer than 7 characters"]);
        //alert("please enter an encryption key longer than 7 characters")
        return;
    }

    document.getElementById('upload-btn').disabled = true;
    document.getElementById('download1').disabled = true;

    ipc.send('selectFile', key);
    //TODO: add file explorer box, use the select file handler
    //TODO: rewrite upload handler to upload each of the file's blocks sequentially
    console.log("sending upload request");
    //ipc.send('upload', "");
    return;

});

// click handler for change profile button
profileChange.addEventListener('click', function (event) {
    let profileName = document.getElementById('profiles').value;
    document.getElementById('cur-profile-name-label').innerText = 'Profile: ' + profileName;
    ipc.send('profileForm', profileName);
});

// Sets the name of the loaded profile on the UI
ipc.on('setprofile', (event, arg) => {
    document.getElementById('cur-profile-name-label').innerHTML = 'Profile: ' + arg;
})

// adds to select profile dropdown
ipc.on('profileAdd', (event, arg) => {
    profileList = arg;
    let selectList = document.getElementById('profiles');
    //clear the table
    selectList.innerHTML = "";

    for (let i = 0; i < profileList.length; i++) {
        var element = document.createElement('option');
        var option = profileList[i];
        element.textContent = option;
        element.value = option;
        selectList.appendChild(element);
    }
})

// Used to change the UI to the queen screen upon initial book
ipc.on('initialsetup', (event, arg) => {
    document.getElementById("files-btn").classList.remove("active");
    document.getElementById("queen-btn").classList.add("active");
    document.getElementById("profiles-btn").classList.remove("active");
    document.getElementById("info-btn").classList.remove("active");
    document.getElementById("files-section").hidden = true;
    document.getElementById("queen-section").hidden = false;
    document.getElementById("profiles-section").hidden = true;
    document.getElementById("info-section").hidden = true;
})

//----------------------- begin mouseover info section ------------------------------
document.getElementById('files-btn').addEventListener("mouseenter", function (event) {
    document.getElementById('mouseover-info').innerText = 'Manage your files';
});
document.getElementById('queen-btn').addEventListener("mouseenter", function (event) {
    document.getElementById('mouseover-info').innerText = 'Edit connection settings';
});
document.getElementById('profiles-btn').addEventListener("mouseenter", function (event) {
    document.getElementById('mouseover-info').innerText = 'Change server connections and manage profiles';
});
document.getElementById('info-btn').addEventListener("mouseenter", function (event) {
    document.getElementById('mouseover-info').innerText = 'In depth overview of the program';
});
document.getElementById('upload-btn').addEventListener("mouseenter", function (event) {
    document.getElementById('mouseover-info').innerText = 'Open file explorer, and pick a file to upload. You must enter an encryption key in the Encryption Key box before uploading';
});
document.getElementById('key-box').addEventListener("mouseenter", function (event) {
    document.getElementById('mouseover-info').innerText = 'Pick a password for the next file you are uploading.  This password is not saved, Please write down your password';
});
document.getElementById('files-label').addEventListener("mouseenter", function (event) {
    document.getElementById('mouseover-info').innerText = 'Lists all previously uploaded files';
});
document.getElementById('download1').addEventListener("mouseenter", function (event) {
    document.getElementById('mouseover-info').innerText = 'Download the currently highlighted file';
});
document.getElementById('delete1').addEventListener("mouseenter", function (event) {
    document.getElementById('mouseover-info').innerText = 'Delete the currently highlighted file';
});
document.getElementById('connection-label').addEventListener("mouseenter", function (event) {
    document.getElementById('mouseover-info').innerText = 'The web address of the server you are connected to';
});
document.getElementById('worker-Id-label').addEventListener("mouseenter", function (event) {
    document.getElementById('mouseover-info').innerText = 'The ID you have been assigned by the server';
});
document.getElementById('usage-bar').addEventListener("mouseenter", function (event) {
    document.getElementById('mouseover-info').innerText = 'The amount of storage you are currently using';
});
document.getElementById('profile-name-label').addEventListener("mouseenter", function (event) {
    document.getElementById('mouseover-info').innerText = 'An easy to remember name for this new connection';
});
document.getElementById('server-address-label').addEventListener("mouseenter", function (event) {
    document.getElementById('mouseover-info').innerText = 'The web address of the server you want to connect to';
});
document.getElementById('server-port-label').addEventListener("mouseenter", function (event) {
    document.getElementById('mouseover-info').innerText = 'The port number that the server is listening to. If unkown, leave as is';
});
document.getElementById('connect-btn').addEventListener("mouseenter", function (event) {
    document.getElementById('mouseover-info').innerText = 'Forms a connection to the specified server above. You must click register before you have access to the storage on the network';
});
document.getElementById('register-btn').addEventListener("mouseenter", function (event) {
    document.getElementById('mouseover-info').innerText = 'You can register after connecting. The Megabyte donation amount will be sent to the server and your connection information will be updated';
});
document.getElementById('donation-box').addEventListener("mouseenter", function (event) {
    document.getElementById('mouseover-info').innerText = 'The number of megabytes you are giving to the network. you will receive approximately 75% of the donated amount in cloud storage';
});
document.getElementById('export-btn').addEventListener("mouseenter", function (event) {
    document.getElementById('mouseover-info').innerText = 'Save your profile to another location for backup or transfer';
});
document.getElementById('import-btn').addEventListener("mouseenter", function (event) {
    document.getElementById('mouseover-info').innerText = 'Import a profile you have previously exported';
});
document.getElementById('profile-switch').addEventListener("mouseenter", function (event) {
    document.getElementById('mouseover-info').innerText = 'Connect to a different server from one of your saved profiles';
});
document.getElementById('cur-profile-name-label').addEventListener("mouseenter", function (event) {
    document.getElementById('mouseover-info').innerText = 'Your nickname for the current server';
});
