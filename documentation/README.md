
HIVE is a p2p based cloud storage solution. There are two main components that make up the system, the queen or server, and the
worker or client. Each worker will connect to each other to form a p2p network that is controlled by the queen. For more info,
see the README.md for the queen and the worker
