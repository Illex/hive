--- Overview ---

This project was the capstone course requirement for the senior year of 2022 at the university of utah and took place over the course of 2 semesters. It represents the final project and coalescence of skills acquired through the univiersites Computer Science Bachelors program.

The team for this project was comprised of 4 individuals

Brian Dong, Aaron Felix, Ben Jamieson, and Ky Lamoureux

This repository is a mirror of the original private repository hosted by the school. Intelectual property rights are maintained by the four original authors and as such have allowed it to be posted here for public reference. Commit history and authorship was not maintained as part of the transfer however the Documentation directory contains a copy of our design document which outlines the assignment of components, the project design goals and constraints as well as our timeline overview.

For reference the designation was as follows

Server: Aaron and Ben
Client: Brian and Ky

