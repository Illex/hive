# HIVE Queen Server

## Compilation

Compilation is done using maven. ([Click here for maven install instructions](https://maven.apache.org/install.html))

Clone the project, navigate into the directory and type:

``` mvn clean compile assembly:single ```

A jar will be output into target/, which you can then run with:

``` java -jar [hive-queen].jar ```

You will need to use Java 11.

## Building the Database

Navigate to the folder with the "db.sql" file, then run:

``` cat src/database/db.sql | sqlite3 hive-queen.db ```

place the resulting file (which will be named hive-queen.db) in the root directory of the execution.

## Using the network viewer

Running the view.html in a browser and connecting to the queen server on port 400 will allow you to view the layout of the network.
