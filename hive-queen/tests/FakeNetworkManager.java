import network.NetworkManager;
import controller.Controller;
import cluster.Worker;
import java.util.ArrayList;
import java.util.function.Function;
import java.io.IOException;
import java.net.Socket;


public class FakeNetworkManager extends NetworkManager {
  private Function<Controller, ArrayList<String>> onListen;
  private Controller controller;
  public ArrayList<String> results;

  public FakeNetworkManager(Function<Controller, ArrayList<String>> onListen) {
    super(null, 0);
    this.onListen = onListen;
    results = new ArrayList<String>();
  }

  public void setController(Controller c) {
    controller = c;
  }

  @Override
  public void listen () throws IOException {
    onListen.apply(controller);
  }

  @Override 
  public Worker getRandomHolder(Worker exclude) {
    if (exclude.id == 0)
      return new Worker(1);
    return new Worker(0);
  }

  @Override
  public void sendToWorker(String msg, Worker worker) {
    results.add(msg);
  }

  @Override
  public void sendToSocket(String msg, Socket socket) {
    results.add(msg);
  }
}
