import controller.Controller;
import cluster.Worker;
import org.junit.Test;
import org.junit.Assert;
import java.io.IOException;
import java.util.regex.Pattern;

/**
 * Test cases for the Controller class.
 */
public class ControllerTests {
  
  /**
   * Helper method for testing controller behavior. 
   * 
   * unregInputs should be a list of strings to be passed into the handleUnregisteredMessage method.
   * 
   * After all of these are passed in, the regInputs will be passed into the handleMessage method
   * along with the associated int from ids. regInputs and ids should be the same length, and ids[i]
   * should contain the desired worker id that regInputs[i] will be associated with.
   *
   * outputs should contain regexes that the output of the controller can be tested against.
   */
  private void testList(String[] unregInputs, String[] regInputs, int[] ids, String[] outputs) throws IOException {
    assert(regInputs.length == ids.length);
    FakeNetworkManager nm = new FakeNetworkManager(controller -> {
      for (String input : unregInputs) {
        controller.handleUnregisteredMessage(input, null);
      }
      for (int i = 0; i < regInputs.length; i++) {
        controller.handleMessage(regInputs[i], new Worker(ids[i]));
      }
      return null;
    });
    
    var c = new Controller(nm);

    try {
      nm.setController(c);

      c.begin();

      Assert.assertEquals(outputs.length, nm.results.size());
      for (int i = 0; i < outputs.length; i++) {
        Assert.assertTrue(String.format("Expected message to match pattern '%s', but got '%s'.",
              outputs[i], nm.results.get(i)),
            Pattern.matches(outputs[i], nm.results.get(i)));
      }
    } finally {
      c.close();
    }
  }

  @Test
  public void testConnection() throws IOException {
    String[] inputs = new String[] {
      "{type:'join', donation: 10}"
    };

    String[] outputs = new String[] {
      "^\\{\"type\":\"acceptWorker\",\"workerID\":\"[0-9]+\"\\}$"
    };

    testList(inputs, new String[]{}, new int[]{}, outputs);
  }

  @Test 
  public void testInvalidJSON() throws IOException {
    String[] inputs = new String[] {
      "{sfdkjlsdkjls"
    };

    String[] outputs = new String[] {
      "^\\{\"type\":\"error\",\"msg\":\"Malformed message\"\\}$"
    };

    testList(inputs, new String[]{}, new int[]{}, outputs);
  }

  @Test
  public void testUnknownMessageType() throws IOException {
    String[] inputs = new String[] {
      "{\"type\":\"hiveSucks\"}"
    };

    String[] outputs = new String[] {
      "^\\{\"type\":\"error\",\"msg\":\"Unknown message type: hiveSucks\"\\}$"
    };

    testList(inputs, new String[]{}, new int[]{}, outputs);
  }

  @Test
  public void testInvalidWorkerID() throws IOException {
    String[] inputs = new String[] {
      "{\"type\":\"connect\",\"ID\":\"abc\"}"
    };

    String[] outputs = new String[] {
      "^\\{\"type\":\"error\",\"msg\":\"ID is not an integer\\.\"\\}$"
    };

    testList(inputs, new String[]{}, new int[]{}, outputs);
  }

  @Test
  public void testPrematureMessages() throws IOException {
    String[] inputs = new String[] {
      "{\"type\":\"upload\"}",
      "{\"type\":\"download\"}",
      "{\"type\":\"delete\"}"
    };

    String[] outputs = new String[] {
      "^\\{\"type\":\"error\",\"msg\":\"You need to register/connect first\\.\"\\}$",
      "^\\{\"type\":\"error\",\"msg\":\"You need to register/connect first\\.\"\\}$",
      "^\\{\"type\":\"error\",\"msg\":\"You need to register/connect first\\.\"\\}$"
    };

    testList(inputs, new String[]{}, new int[]{}, outputs);
  }

  @Test
  public void testBadDownload() throws IOException {
    String[] unregInputs = new String[] {
      "{\"type\":\"connect\",\"ID\":\"2\"}"
    }; 

    String[] regInputs = new String[] {
      "{\"type\":\"download\"}"
    };

    int[] ids = new int[] {2};

    String[] outputs = new String[] {
      "^\\{\"type\":\"ok\"\\}$",
      "^\\{\"type\":\"error\",\"msg\":\"No file id specified.\"\\}$"
    };

    testList(unregInputs, regInputs, ids, outputs);
  }

  @Test
  public void testBadUpload() throws IOException {
    String[] unregInputs = new String[] {
      "{\"type\":\"connect\",\"ID\":\"2\"}"
    }; 

    String[] regInputs = new String[] {
      "{\"type\":\"upload\"}"
    };

    int[] ids = new int[] {2};

    String[] outputs = new String[] {
      "^\\{\"type\":\"ok\"\\}$",
      "^\\{\"type\":\"error\",\"msg\":\"No file id specified.\"\\}$"
    };

    testList(unregInputs, regInputs, ids, outputs);
  }


}
