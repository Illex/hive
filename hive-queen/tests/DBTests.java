
import static org.junit.Assert.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.UUID;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import cluster.Worker;
import database.DataBlock;
import database.DatabaseManager;

public class DBTests {

  @BeforeClass
  public static void setUpBeforeClass() throws Exception {}

  @AfterClass
  public static void tearDownAfterClass() throws Exception {}

  @Before
  public void setUp() throws Exception {}

  @After
  public void tearDown() throws Exception {}

  public void clearDB() {
    try {
      Connection connection = DriverManager.getConnection(DatabaseManager.SQL_CONN);
      connection.createStatement().executeUpdate("DELETE FROM blocks");
      connection.createStatement().executeUpdate("DELETE FROM holders");
      connection.createStatement().executeUpdate("DELETE FROM workers");
      connection.createStatement().executeUpdate("DELETE FROM SQLITE_SEQUENCE WHERE name='blocks'");
      connection.createStatement().executeUpdate("DELETE FROM SQLITE_SEQUENCE WHERE name='holders'");
      connection.createStatement().executeUpdate("DELETE FROM SQLITE_SEQUENCE WHERE name='workers'");
      connection.close();
    } catch (SQLException e) {
      e.printStackTrace();
      fail();
    }
    
  }
  
  @Test
  public void MakeDBManager() {
    DatabaseManager db = new DatabaseManager();
    db.CloseConnection();
  }
  
  @Test
  public void TestDataBlockDBFuncs() 
  {
    clearDB();
    DatabaseManager db = new DatabaseManager();
    
    //prereqs
    UUID id = UUID.randomUUID();
    Worker owner = new Worker(4);
    
    //make into datablock
    boolean success = db.blocks.addBlock(id, new byte[256], owner);
    DataBlock b = db.blocks.getBlock(id);    
    
    assertTrue(success);
    
    
    //test holdership adding
    Worker holder1 = new Worker(1);
    Worker holder2 = new Worker(2);
    //store one and retrieve
    b.addHolder(holder1);
    assertEquals(b.getRandomHolder(), holder1);
    assertEquals(b.getAllHolders().size(), 1);
    
    //increases in size
    b.addHolder(holder2);
    assertEquals(b.getAllHolders().size(), 2);
    
    //decreases in size and deletes
    b.removeHolder(holder1);
    assertEquals(b.getAllHolders().size(), 1);
    //reasonable doubt that its not
    assertNotEquals(b.getRandomHolder(), holder1);
    assertNotEquals(b.getRandomHolder(), holder1);
    assertNotEquals(b.getRandomHolder(), holder1);
    assertNotEquals(b.getRandomHolder(), holder1);
    
    
    //make sure the right one is in there
    assertEquals(b.getAllHolders().get(0), holder2);
    
    //delete the last one
    b.removeHolder(b.getRandomHolder());
    
    
    assertEquals(b.getRandomHolder(), null);
    
    //test deleting
    b.delete();
    assertNull(db.blocks.getBlock(id));
    
    db.CloseConnection();
    
  }
  
  @Test
  public void TestWorkerDBFuncs() {
    clearDB();
    //adding a worker
    DatabaseManager db = new DatabaseManager();
    int workerId = db.workers.addWorker(12);
    //stats line up for fresh worker
    assertEquals(db.workers.getWorkerDonatedBlocks(new Worker(workerId)),12);
    assertEquals(db.workers.getWorkerUsedBlocks(new Worker(workerId)),0);
    
    int worker2Id = db.workers.addWorker(22);
    //stats line up for fresh worker
    assertEquals(db.workers.getWorkerDonatedBlocks(new Worker(workerId)),22);
    assertEquals(db.workers.getWorkerUsedBlocks(new Worker(workerId)),0);
    
    
    //assertEquals(db.replaceableLocations.getWorkerWithReplaceableBlock(), new HeldBlock(workerId));
    
    assertEquals(db.workers.getAllWorkers().spliterator().getExactSizeIfKnown(),1);
    
    //updating used blocks
    db.workers.setWorkerUsedBlocks(new Worker(workerId), 11);
    assertEquals(db.getWorkerUsedBlocks(workerId),11);
    
    //updating donated blocks
    db.workers.setWorkerDonatedBlocks(new Worker(workerId),15);
    assertEquals(db.getWorkerDonatedBlocks(workerId),15);
    db.CloseConnection();
  }

}
