package webvisual;

import java.net.InetSocketAddress;
import java.util.Collection;
import java.util.List;
import org.java_websocket.WebSocket;
import org.java_websocket.drafts.Draft;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import cluster.ClusterManager;
import cluster.Worker;
import database.DataBlock;
import database.DatabaseManager;
import network.NetworkManager;

public class ClientCommunicator extends WebSocketServer {

  
  NetworkManagerV currentNet;
  DatabaseManager currentDb;
  ClusterManager currentCluster;
  public ClientCommunicator(int port, NetworkManagerV currentNetwork, DatabaseManager currentDb, ClusterManager currentCluster) {
    super(new InetSocketAddress("localhost",port));
    this.currentNet = currentNetwork;
    this.currentDb = currentDb;
    this.currentCluster = currentCluster;
    super.start();
    System.out.println("Running with network visualizer on port "+port);
  }
  

  public String netStateToJson(NetworkManagerV nm, DatabaseManager db) {
    
    //first, build workers collection
    String json = "{ \"msg\": {\"type\":\"init\", \"workers\": [";
    for(Worker w : db.workers.getAllWorkers()) {
      String worker = "{ \"id\": "+w.id+", \"online\":";
      if(nm.getOnlineWorkers().contains(w)) {
        worker += "true";
      }else {
        worker += "false";
      }
      worker += ',';
      worker += "\"heldBlocks\": [";
      //for this worker, give it its files
      for(DataBlock b : db.blockLocations.getWorkerHeldBlocks(w)) {
        worker += "\" "+b.ownerID+"'s "+b.id.toString()+"\",";
      }
      worker += "],";
      
      worker += "},";
      json += worker;
    }
    json += "]}}";
    
    json = json.replaceAll(",}", "}");
    json = json.replaceAll(",]", "]");
    return json;
  }

  @Override
  public void onOpen(WebSocket conn, ClientHandshake handshake) {
    // TODO Auto-generated method stub
    System.out.println("new visualizer client connected");
    conn.send(netStateToJson(currentNet,currentDb));
    
  }

  @Override
  public void onClose(WebSocket conn, int code, String reason, boolean remote) {
    // TODO Auto-generated method stub

  }

  @Override
  public void onMessage(WebSocket conn, String message) {
    // TODO Auto-generated method stub

  }

  @Override
  public void onError(WebSocket conn, Exception ex) {
    // TODO Auto-generated method stub
    System.out.println("error:" + ex.toString());

  }

  @Override
  public void onStart() {
    // TODO Auto-generated method stub

  }

}
