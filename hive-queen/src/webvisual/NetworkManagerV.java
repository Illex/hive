package webvisual;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Collection;
import cluster.ClusterManager;
import cluster.Worker;
import controller.Controller;
import database.DatabaseManager;
import network.NetworkManager;


public class NetworkManagerV extends NetworkManager {
  ClientCommunicator c;
  int port;
  public NetworkManagerV(Controller c, DatabaseManager db, ClusterManager cm, int port) {
    super(c, port);
    this.port = port;
    this.c = new ClientCommunicator(400, this,db,cm);
    
    // TODO Auto-generated constructor stub
  }
  
 /* @Override
  public void listen() throws IOException {
    var server = new ServerSocket(port);

    while (true) {
      (new SocketHandlerV(server.accept())).start();
    }
  }*/
  
  //todo: refactor the structure of this.
  //this is because we are out of time.\
  //this should not exist.
  public void sendToV(String msg) {
    c.broadcast(msg);
  
  }
  
  
  @Override
  public void sendToSocket(String msg, Socket socket) {
    super.sendToSocket(msg, socket);
    int sentToWorkerId = super.workers.get(socket).id;
    
    String wrappedMsg = "{\"msg\": "+msg+",\"sentTo\":"+sentToWorkerId+"}";
    
    
    c.broadcast(wrappedMsg );
    
   // System.out.println(wrappedMsg);
    
  }
  
  /*
  private class SocketHandlerV extends NetworkManager.SocketHandler{

    public SocketHandlerV(Socket s) {
      super(s);
      // TODO Auto-generated constructor stub
    }
    @Override
    public void run() {
      super.run();
      c.broadcast(null);
    
    }
  }
  */
 
  
  public Collection<Worker> getOnlineWorkers() {
    return super.workers.values();
  }
  
  

}
