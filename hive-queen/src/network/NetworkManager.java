package network;

import cluster.Worker;
import controller.Controller;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.Random;
import java.util.Arrays;

public class NetworkManager {
  private final HashMap<Worker, Socket> sockets;
  protected final HashMap<Socket, Worker> workers;
  private final HashMap<Worker, Socket> traversalSockets;
  private final int port;
  private final Controller controller;

  /** Lock for restricting access to tables containing worker and socket info. */
  private final Lock tableLock;
  private final Lock traversalLock;

  public NetworkManager(Controller c, int port) {
    sockets = new HashMap<Worker, Socket>();
    workers = new HashMap<Socket, Worker>();
    traversalSockets = new HashMap<Worker, Socket>();
    controller = c;

    tableLock = new ReentrantLock();
    traversalLock = new ReentrantLock();

    this.port = port;
  }

  
  public void listen() throws IOException {
    var server = new ServerSocket(port);

    while (true) {
      (new SocketHandler(server.accept())).start();
    }
  }

  public Worker getRandomHolder(Worker exclude) {
    if (workers.size() < 2) {
      return null;
    }

    Random rand = new Random();
    int count = 0;
    int stop = rand.nextInt(workers.size());

    // TODO get rid of infinite loop.
    for (Worker w : workers.values()) {
      if (count == stop) {
        if (w != exclude) {
          return w;
        }
        else {
          return getRandomHolder(exclude);
        }
      }
      count++;
    }

    return null;
    //throw new IllegalStateException("No workers available.");
  }

  public boolean isConnected(Worker worker) {
    return sockets.get(worker) != null;
  }
  
  public int workerCount() {
    return sockets.size();
  }

  public void bindWorker(Worker worker, Socket socket) {
    tableLock.lock();
    try {
      sockets.put(worker, socket);
      workers.put(socket, worker);
    } finally {
      tableLock.unlock();
    }
  }

  public void disconnectWorker(Worker worker) {
    tableLock.lock();
    try {
      Socket s = sockets.get(worker);
      workers.remove(s);
      sockets.remove(worker);
      s.close();
    } catch (IOException e) {
      // Something happened when closing the socket. I don't think anything needs to be done.
    } finally {
      tableLock.unlock();
    }
  }

  public void disconnectSocket(Socket socket) {
    tableLock.lock();
    try {
      Worker w = workers.get(socket);
      sockets.remove(w);
      workers.remove(socket);
      socket.close();
    } catch (IOException e) {
      // Do nothing.
    } finally {
      tableLock.unlock();
    }
  }

  public void sendToWorker(String msg, Worker worker) {
    Socket socket = sockets.get(worker);
    if (socket == null) {
      throw new IllegalArgumentException("Worker is not connected.");
    }
    sendToSocket(msg, socket);
  }

  public void sendToSocket(String msg, Socket socket) {
    try {
      //System.out.format("Sending message to %s:%d:\n '%s'\n", socket.getInetAddress().getHostAddress(), socket.getPort(), msg);
      // Add length prefix and send message.
      byte[] ascii = msg.getBytes("US-ASCII");

      StringBuilder builder = new StringBuilder();
      builder.append(Integer.toString(ascii.length + 1)); // Plus one for the newline.
      builder.append("|");
      builder.append(msg);
      builder.append("\n");

      socket.getOutputStream().write(builder.toString().getBytes());
    } catch (IOException e) {
      disconnectSocket(socket);
    }
  }

  public void sendToTraversalSocket(String msg, Worker worker) {
    Socket socket = traversalSockets.get(worker);
    
    //System.out.format("Sending message to %s:%d:\n '%s'\n", socket.getInetAddress().getHostAddress(), socket.getPort(), msg);
    
    try {
      byte[] ascii = msg.getBytes("US-ASCII");
      socket.getOutputStream().write(ascii);
    } catch (IOException e) {
      System.out.println("Problem sending to traversal socket.");
      e.printStackTrace();
      //TODO handle this.
    }
  }

  protected class SocketHandler implements Runnable {
    private Socket socket;

    public SocketHandler(Socket s) {
      socket = s;
    }

    @Override
    public void run() {
      // One message could take multiple tcp packets, so this isn't perfect.
      // Also readAllBytes is a blocking method which is not ideal for scalability
      // if we want queens to be connected to many workers at once.

      System.out.format("New connection from %s:%d.\n", socket.getInetAddress().getHostAddress(), socket.getPort());

      byte[] buffer = new byte[512];
      long messageLength = 0;
      long bytesRead = 0;
      StringBuilder builder = new StringBuilder();

      try {
        var input = socket.getInputStream();
        //We probably want a separate method in the controller for if we're receiving
        //a message from a worker we don't know yet.
        while (!socket.isClosed()) {
          if (messageLength == 0) {
            ByteBuffer byteBuf = ByteBuffer.allocate(12);
            int strLength = 0;

            byte[] next;

            while (true) {
              next = input.readNBytes(1);
              if (next[0] == '|')
                break;
              byteBuf.put(next);
              strLength++;
            }

            // TODO catch NumberFormat exception
            messageLength = Long.parseLong(new String(Arrays.copyOfRange(byteBuf.array(), 0, strLength)));
          }

          int length = input.read(buffer);

          // Check if they disconnected.
          if (length == -1) {
            disconnectSocket(socket);
            break;
          }

          builder.append(new String(buffer, 0, length, "US-ASCII"));
          bytesRead += length;

          if (bytesRead >= messageLength) {
            bytesRead = 0;
            messageLength = 0;

            String message = builder.toString();

            builder = new StringBuilder();

            //System.out.format("Received message from %s:%d:\n '%s'\n", socket.getInetAddress().getHostAddress(), socket.getPort(), message);

            tableLock.lock();
            try {
              if (workers.get(socket) != null) {
                controller.handleMessage(message, workers.get(socket));
              } else {
                controller.handleUnregisteredMessage(message, socket);
              }
            } finally {
              tableLock.unlock();
            }
          }
        }
      } catch (IOException e) {
        disconnectSocket(socket);
      }
    }

    public void start() {
      (new Thread(this, socket.toString())).start();
    }
  }
}
