package cluster;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.UUID;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import database.DataBlock;
import database.DatabaseManager;
import database.HeldBlock;
import network.NetworkManager;
import controller.Controller;

public class ClusterManager {

  private final DatabaseManager db;
  private final Controller c;
  private final ScheduledExecutorService uptimeCheckExecutor;
  private int uptimeCheckInterval = 15 * 60; // every 15 minutes 
  private static final double UPTIME_SCORE_MIN = 0.9;
  
  private static final double VALIDATION_SCORE_MIN = 0.9;
  
  private final ScheduledExecutorService blockValidatorExecutor;
  //private static final int BLOCKVALIDATOR_INTERVAL = 60*60*4; //every 4 hours
  private int blockValidatorRoundTime = 24 * 60 * 7 * 60;//every week
  static int replicationFactor;    
  

  public ClusterManager(DatabaseManager db, Controller c, int replicationFactor, int uptimeInterval, int validateInterval) {
    this.replicationFactor = replicationFactor;
    this.db = db;
    this.c = c;
    uptimeCheckInterval = uptimeInterval;
    blockValidatorRoundTime = validateInterval;
    uptimeCheckExecutor = Executors.newScheduledThreadPool(1);
    uptimeCheckExecutor.scheduleAtFixedRate(new UptimeChecker(), uptimeCheckInterval, uptimeCheckInterval, TimeUnit.SECONDS);

    
    blockValidatorExecutor = Executors.newScheduledThreadPool(1);
    blockValidatorExecutor.scheduleAtFixedRate(new BlockValidator(), uptimeCheckInterval, uptimeCheckInterval, TimeUnit.SECONDS);
  }

  
  /**
   * Creates a new worker with numBlocks free blocks and returns the new worker.
   */
  public Worker newWorker(int numBlocks) throws Exception {

    // Add the worker to the database.

    int id = db.workers.addWorker(numBlocks);

    if (id == -1) {
        throw new Exception("Could not add worker.");
    }

    return new Worker(id);
  }
  
  public void banWorker(Worker w) {
    //todo:   add to 'banned workers' to send message on connection attempt..
    /*
     * 1. add to 'banned workers' list. this list needs to be checked against when a worker tries to connect. 
     * 2. disconnect worker.
     * 3. delete worker entry from database. mark blocks as timing out to deletion.
     * 4. check redundancies.
     */
   
    System.out.format("Banning worker %d.\n", w.id); 
   
    System.out.println("Redistributing their blocks...");
    for(DataBlock b : db.blockLocations.getWorkerHeldBlocks(w)) {
      new Thread(() -> {c.redistBlock(b);}).run();
    }

    System.out.println("Done redistributing.");
    db.workers.addWorkerToBannedList(w);//todo: garbage collection daily or so (on startup too).
    //also looks for blocks we need
    
    //todo db.isbanned(w)  
    
    c.disconnectWorker(w);
   
    // TODO ban workers better
    //3 and 4
    //db.clearWorkerData(w);
    
    
    
  }
  
  public void validationFailure(Worker w, UUID blockID) {
    db.workers.updateWorkerBlockScore(w, false);
    if(db.workers.getWorkerBlockScore(w) < VALIDATION_SCORE_MIN) {
      new Thread(() -> {banWorker(w);}).start();
    }
    c.sendWarning(w);
  }

  public void validationResponse(Worker w, UUID blockID, byte[] block) {
    System.out.format("Received validation response from worker %d for block %s.\n", w.id, blockID);
    
    // Hash the block
    MessageDigest md;
    try {
      md = MessageDigest.getInstance("SHA-256");
    }catch(Exception e) {
      System.err.println("FATAL ERROR: sha256 setup fail");
      e.printStackTrace();
      System.exit(-1);
      return;
    }
    
    
    byte[] actualHash = md.digest(block);
    
    // Check hash and respond appropriately.
    if (Arrays.equals(db.blocks.getBlock(blockID).sha, actualHash)) { 
      System.out.println("Validation successful.");
      db.workers.updateWorkerBlockScore(w, true);
    } else {
      System.out.println("Validation unsuccessful.");
      validationFailure(w, blockID);
    }
  }
  
  
  private class UptimeChecker implements Runnable{

    @Override
    public void run() {
      for (Worker w : db.workers.getAllWorkers()) {
         
        boolean isConnected = c.isConnected(w);      
        db.workers.updateWorkerUptimeScore(w, isConnected);
        
        if(!isConnected && db.workers.getWorkerUptimeScore(w) < UPTIME_SCORE_MIN) {
          
          new Thread(() -> {banWorker(w);}).start();
        }
         
      }
      
    }
    
  }
  
  private class BlockValidator implements Runnable{

    @Override
    public void run() {
      try {
        // TODO only select workers that were online when last checked. Also will need looping here with offset.
        System.out.println("Validating random block.");
        ArrayList<HeldBlock> oldestTen = db.blockLocations.getStalestHeldBlocks(10);
        HeldBlock toValidate = oldestTen.get((int)(Math.random() * 10));
        c.sendValidationRequest(new Worker(toValidate.HolderID), toValidate.blockID);
        db.blockLocations.updateLastChecked(toValidate);
      }finally {
        //check an amount such that checking that amount per interval will go through every block by a time period
        int blockCount = db.blockLocations.networkCapacity();
        int delay = blockValidatorRoundTime / blockCount;
        blockValidatorExecutor.schedule(this, delay, TimeUnit.SECONDS);
      }
    }
    
  }
  
}
