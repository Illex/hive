package cluster;

public class Worker {
  public final int id;

  public Worker(int id) {
    this.id = id;
  }

  @Override
  public int hashCode() {
    return id;
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof Worker))
      return false;
    if ((((Worker) o).id == this.id))
      return true;
    else
      return false;

  }
}
