package controller;

import cluster.ClusterManager;
import cluster.Worker;
import database.HeldBlock;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import database.DataBlock;
import database.DatabaseManager;
import java.io.IOException;
import java.net.Socket;
import java.util.UUID;
import network.NetworkManager;
import webvisual.ClientCommunicator;
import webvisual.NetworkManagerV;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.security.MessageDigest;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.Arrays;

/** communication between components. */
public class Controller {

  // Timeout for worker responses in milliseconds.
  private static final int RESPONSE_TIMEOUT = 10000;
  private static int replicationFactor;
  private final Lock recordLock;

  protected final NetworkManager nm;
  private final DatabaseManager db;
  private final ClusterManager cm;

  // For keeping track of timeouts and downloadRequests
  private HashMap<Holder, ScheduledExecutorService> validateTimeouts;
  private HashMap<Holder, ScheduledExecutorService> acceptTimeouts;
  private HashMap<Holder, UUID> overwrittenBlocks;

  //make 3 different hash tables for the different download requests
  //the main request ledger to keep track of our active downloads
  private HashMap<Holder, Worker> downloadRequests;
  //TODO: change these three tables to use a record where the key is the main uuid, and the value is the sub uuid + sub block content
  private HashMap<Holder, Worker> downloadRequestsA;
  private HashMap<Holder, Worker> downloadRequestsB;
  private HashMap<Holder, Worker> downloadRequestsC;

  private HashMap<Worker, BlockContainer> downloadRecord;


  private HashMap<Holder, Holder> redistTable;
  private static final boolean ViewerEnabled = true;

  /**
   * instansiate a controller.
   *
   * @param networkManager The network manager to be used by this controller.
   */
  public Controller(int port, int replicationFactor, int uptimeInterval, int validateInterval) {
    this.replicationFactor = replicationFactor;
    
    if(ViewerEnabled) {
      db = new DatabaseManager(replicationFactor);
      cm = new ClusterManager(db, this, replicationFactor, uptimeInterval, validateInterval);
      nm = new NetworkManagerV(this,db, cm, port);
      
    }else {
      nm = new NetworkManager(this, port);
      db = new DatabaseManager(replicationFactor);
      cm = new ClusterManager(db, this, replicationFactor, uptimeInterval, validateInterval);
    }

    validateTimeouts = new HashMap<Holder, ScheduledExecutorService>();
    acceptTimeouts = new HashMap<Holder, ScheduledExecutorService>();
    overwrittenBlocks = new HashMap<Holder, UUID>();
    downloadRequestsA = new HashMap<Holder, Worker>();
    downloadRequestsB = new HashMap<Holder, Worker>();
    downloadRequestsC = new HashMap<Holder, Worker>();
    downloadRecord = new HashMap<Worker, BlockContainer>();
    redistTable = new HashMap<Holder, Holder>();
    recordLock = new ReentrantLock();
  }

  /*
   * Alternate constructor for testing purposes only.
   */
  public Controller(NetworkManager nm) {
    this.nm = nm;
    db = new DatabaseManager(1);
    cm = new ClusterManager(db, this, 1, 900, 604800);

    validateTimeouts = new HashMap<Holder, ScheduledExecutorService>();
    acceptTimeouts = new HashMap<Holder, ScheduledExecutorService>();
    overwrittenBlocks = new HashMap<Holder, UUID>();
    downloadRequestsA = new HashMap<Holder, Worker>();
    downloadRequestsB = new HashMap<Holder, Worker>();
    downloadRequestsC = new HashMap<Holder, Worker>();
    downloadRecord = new HashMap<Worker, BlockContainer>();
    redistTable = new HashMap<Holder, Holder>();
    recordLock = new ReentrantLock();
  }

  public void begin() throws IOException {
    nm.listen(); 
  }

  public void close() {
    db.CloseConnection();
  }

  /**
   * Direct a worker action to the correct module after parsing it out.
   *
   * @param msg Json object. root must contain a "type" field
   * @param worker
   */
  public void handleMessage(String msg, Worker worker) {
    // Parse json from the message and call the appropriate methods.
    Gson gson = new Gson();

    String messageType;
    try {
      messageType = getMessageType(msg);
    } catch (JsonSyntaxException e) {
      nm.sendToWorker(gson.toJson(new ErrorMessage("Malformed message")), worker);
      return;
    }

    switch (messageType) {
      case "upload":

        System.out.println("printing the content string in upload");
        //get the data array section of the json string
        String contentString = msg.split(":")[4];
        //System.out.println(msg.split(":")[4]);

        //split out the extra brackets
        contentString = contentString.split("}")[0];

        //System.out.println("printing the split string in upload case");
        //System.out.println(contentString);

        //send the string to a function that will take it and return two byte arrays
        //
        //the 3 sub block arrays, subC will be used for the parity array
        String [] subA = null;
        String [] subB = null;
        String [] subC = null;

        String [][] temp = null;
        boolean f = false;

        //get all the numbers in string form
        String[] contentArr = contentString.substring(1,contentString.length() -1).split(",");
        String[] other = null; 

        if(contentArr.length % 2 != 0){
            f = true;
            other = new String[contentArr.length+1];
            for(int i = 0; i < contentArr.length; i++){
                other[i] = contentArr[i];
            }
            other[other.length -1] = "0";
        }
        else{
            other = contentArr;
        }
        

        //this boolean is used to keep track of if the block length was even or odd. it will be added to the table
        //split the given block into the two sub blocks and return them in an array of byte arrays
        temp = splitBlock(other);

        subA = temp[0];
        subB = temp[1];

        //get the parity block for the two sub blocks
        //System.out.println("array A should now have half the content");
        //System.out.println(Arrays.toString(subA));

        //accually assign the c value
        subC = genParity(subA, subB);
        //System.out.println("the generatied parity array");
        //System.out.println(Arrays.toString(subC));

        //make sure they have enough space.
        //TODO: make sure 3 workers have enough space
        if(db.workers.getWorkerDonatedBlocks(worker) - db.workers.getWorkerUsedBlocks(worker) < replicationFactor) {
          nm.sendToWorker(gson.toJson(new ErrorMessage("You are using all your space on the network.")), worker);
          break;
        }
        

        // Randomly choose workers to send block to. 
        if(nm.workerCount() < replicationFactor) {
          nm.sendToWorker(gson.toJson(new ErrorMessage("Not enough connected workers! (less than replicationFactor)")), worker);
          break;
        }
        
        ArrayList<HeldBlock> newHoldLocations = new ArrayList<HeldBlock>();
        
        for(int i = 0; i < replicationFactor; ) {
          //build the list of workers we don't want our new blocks to be in
          ArrayList<Worker> excludes = new ArrayList<Worker>(newHoldLocations.size() + 1);
          excludes.add(worker);
          for(HeldBlock h : newHoldLocations)
            excludes.add(new Worker(h.HolderID));
          
          //get a holder not from the exclude list
          HeldBlock potentialHolder = db.replaceableLocations.getWorkerWithReplaceableBlock(excludes);
          if (nm.isConnected(new Worker(potentialHolder.HolderID))) {
            newHoldLocations.add(potentialHolder);
            i++;
          }
          
        }


        if (newHoldLocations.size() < replicationFactor) {
          nm.sendToWorker(gson.toJson(new ErrorMessage("No available holders.")), worker);
          break;
        }

        // create a file ID.
        UUID newBlockID = UUID.randomUUID();
        UUID subAID = UUID.randomUUID();
        UUID subBID = UUID.randomUUID();
        UUID subCID = UUID.randomUUID();

        System.out.println("making the 3 messages");
        //generate new upload messages
        String msgA = genMessage(msg, subA);
        String msgB = genMessage(msg, subB);
        String msgC = genMessage(msg, subC);

        //System.out.println("resulting message strings");
        //System.out.println(msgA);
        //System.out.println(msgB);
        //System.out.println(msgC);

        //regenerate the 3 upload messages
        UploadMessage uploadMessageA = gson.fromJson(msgA, UploadMessage.class);
        UploadMessage uploadMessageB = gson.fromJson(msgB, UploadMessage.class);
        UploadMessage uploadMessageC = gson.fromJson(msgC, UploadMessage.class);

        int counter = 0;

        // Upload the file to the newHolder. 
        // only works with replication factor 3
        for(HeldBlock newHolder : newHoldLocations) {
            //System.out.println("sending message number: " + Integer.toString(counter));
        
          final Worker selectedHolder = new Worker(newHolder.HolderID);
          if(counter == 0){
            nm.sendToWorker(gson.toJson(new StoreFileMessage(subAID, newHolder.blockID, uploadMessageA.content.data)), selectedHolder);
            Holder storage = new Holder(newHolder.HolderID, subAID);
            overwrittenBlocks.put(storage, newHolder.blockID);

          }
          else if (counter == 1){
            nm.sendToWorker(gson.toJson(new StoreFileMessage(subBID, newHolder.blockID, uploadMessageB.content.data)), selectedHolder);
            Holder storage = new Holder(newHolder.HolderID, subBID);
            overwrittenBlocks.put(storage, newHolder.blockID);
          }
          else{
            nm.sendToWorker(gson.toJson(new StoreFileMessage(subCID, newHolder.blockID, uploadMessageC.content.data)), selectedHolder);
            Holder storage = new Holder(newHolder.HolderID, subCID);
            overwrittenBlocks.put(storage, newHolder.blockID);
          }
          counter++;
        }

        // TODO check for errors when doing database stuff.
        MessageDigest md;
        try {
          md = MessageDigest.getInstance("SHA-256");
        }catch(Exception e) {
          System.err.println("FATAL ERROR: sha256 setup fail");
          e.printStackTrace();
          System.exit(-1);
          return;
        }

        System.out.println("adding 3 sub blocks to database");
        //add the sub blocks to the normal block table
        db.blocks.addBlock(subAID, md.digest(uploadMessageA.content.data), worker);
        db.blocks.addBlock(subBID, md.digest(uploadMessageB.content.data), worker);
        db.blocks.addBlock(subCID, md.digest(uploadMessageC.content.data), worker);

        //put the main block id into the translation table

        System.out.println("adding 3 sub block mapping record");
        db.blocks.addSubBlocks(newBlockID, subAID, subBID, subCID, f);

        // For now just send back the id immeadiately. 
        System.out.println("sending id back to worker");
        nm.sendToWorker(gson.toJson(new UploadAcceptMessage(newBlockID)), worker);

        break;
      case "download": // findBlock might be more appropriate
        DownloadMessage downloadMessage = gson.fromJson(msg, DownloadMessage.class);

        if(!downloadMessage.isValid()) {
          nm.sendToWorker(gson.toJson(new ErrorMessage("Invalid file id.")), worker); 
          break;
        }


        //get the sub block translation record
        UUID[] subID = db.blocks.getSubBlocks(UUID.fromString(downloadMessage.fileID));

        //get the sub block from the blocks table
        DataBlock toDownload1 = db.blocks.getBlock(subID[0]);
        DataBlock toDownload2 = db.blocks.getBlock(subID[1]);
        DataBlock toDownload3 = db.blocks.getBlock(subID[2]);
        
        //get the list of holders for each sub block
        ArrayList<Worker> holders1 = toDownload1.getAllHolders();
        ArrayList<Worker> holders2 = toDownload2.getAllHolders();
        ArrayList<Worker> holders3 = toDownload3.getAllHolders();

        ArrayList<Worker> onlineHoldersA = new ArrayList<Worker>();
        ArrayList<Worker> onlineHoldersB = new ArrayList<Worker>();
        ArrayList<Worker> onlineHoldersC = new ArrayList<Worker>();

        for(Worker holder : holders1) {
          if (nm.isConnected(holder))
            onlineHoldersA.add(holder);
        }
        for(Worker holder : holders2) {
          if (nm.isConnected(holder))
            onlineHoldersB.add(holder);
        }
        for(Worker holder : holders3) {
          if (nm.isConnected(holder))
            onlineHoldersC.add(holder);
        }
          
        if(onlineHoldersA.size() == 0 || onlineHoldersB.size() == 0 || onlineHoldersC.size() == 0 ) {
          nm.sendToWorker(gson.toJson(new ErrorMessage("no holders online.")), worker);
          break;
        }
          
        Worker holderA = onlineHoldersA.get(0);
        Worker holderB = onlineHoldersB.get(0);
        Worker holderC = onlineHoldersC.get(0);
        
        Holder requestedBlockA = new Holder(holderA.id, toDownload1.id);
        Holder requestedBlockB = new Holder(holderB.id, toDownload2.id);
        Holder requestedBlockC = new Holder(holderC.id, toDownload3.id);
        
        //worker is the download requester?
        downloadRequestsA.put(requestedBlockA, worker);
        downloadRequestsB.put(requestedBlockB, worker);
        downloadRequestsC.put(requestedBlockC, worker);

        //now that the 3 sub blocks are in 3 hash tables we can check for entries from all 3 before trying to reconstruct the message
        nm.sendToWorker(gson.toJson(new FileRequestMessage(toDownload1.id)), holderA);
        nm.sendToWorker(gson.toJson(new FileRequestMessage(toDownload2.id)), holderB);
        nm.sendToWorker(gson.toJson(new FileRequestMessage(toDownload3.id)), holderC);

        //add a downloadRecord entry to place the blocks as they come back from the storing workers
        downloadRecord.put(worker, new BlockContainer());


        break;
      case "delete":
        DeleteMessage deleteMessage = gson.fromJson(msg, DeleteMessage.class);
        // Update database.
        
        for (String id : deleteMessage.uuids) {
          try {
              //TODO:
              //add a section to get the 3-sub blocks from the mapping table and delete them as well
            UUID blockID = UUID.fromString(id);
            UUID[] subIDS = null;

            //DataBlock toDelete = db.blocks.getBlock(blockID);
            //get the 3 sub block ids from the mapping table and then get their respective stored blocks
            //TODO: implement getSubBlocks in the db manager
            subIDS = db.blocks.getSubBlocks(blockID);

            //there are 3 ids, one for each sub block
            DataBlock toDelete1 = db.blocks.getBlock(subIDS[0]);
            DataBlock toDelete2 = db.blocks.getBlock(subIDS[1]);
            DataBlock toDelete3 = db.blocks.getBlock(subIDS[2]);


            if (toDelete1 == null || toDelete2 == null ||toDelete3 == null  ) {
              nm.sendToWorker(gson.toJson(new ErrorMessage("Invalid block id.")), worker);
              break;
            }

            //delete all 3 stored sub blocks
            toDelete1.delete();
            toDelete2.delete();
            toDelete3.delete();

            //remove the translation record
            db.blocks.removeSubBlocks(blockID);

            
            //todo THIS IS WRONG!!!
            if(ViewerEnabled) {
              ((NetworkManagerV)nm).sendToV("{\"msg\":{\"type\": \"delete\", \"toDelete\":[\""+
            subIDS[0]+"\", \""+
            subIDS[1]+"\", \""+
            subIDS[2]
            +"\"]}}");
            }
            
            
          } catch (IllegalArgumentException e) {
            nm.sendToWorker(gson.toJson(new ErrorMessage("Bad uuid.")), worker);
            break;
          }
        }
        break;
      case "fileAccept":
        // Stop timer from timing out.
        FileAcceptMessage acceptMessage = gson.fromJson(msg, FileAcceptMessage.class);
        Holder accepter = new Holder(worker.id, UUID.fromString(acceptMessage.fileID));

        ScheduledExecutorService acceptTimeout = acceptTimeouts.get(accepter);
        if (acceptTimeout != null) {
          acceptTimeout.shutdownNow();
          acceptTimeouts.remove(accepter);
        }


        // Add holder to database
        DataBlock addedBlock = db.blocks.getBlock(UUID.fromString(acceptMessage.fileID));
        addedBlock.addHolder(worker);


        UUID oldBlockID = overwrittenBlocks.get(accepter);
        if (oldBlockID != null) {
          System.out.format("Attempting to remove overwritten block: %s\n", oldBlockID);
          db.replaceableLocations.deleteReplaceableBlock(oldBlockID, worker.id);
          overwrittenBlocks.remove(accepter);
        }


        break;
      case "fileResponse":
        FileResponseMessage response = gson.fromJson(msg, FileResponseMessage.class);

        if (!response.isValid()) {
          nm.sendToWorker(gson.toJson(new ErrorMessage("Invalid file response.")), worker);
          break;
        }
        
        //this is the file owner? not important for my raid implementation
        Holder uploader = new Holder(worker.id, UUID.fromString(response.fileID));

        ScheduledExecutorService validateTimeout = validateTimeouts.get(uploader);
        if (validateTimeout != null) {
          validateTimeout.shutdownNow();
          validateTimeouts.remove(uploader);
        }

        // We can probably just do a validation every time we receive the file to save time later.
        new Thread(() -> {cm.validationResponse(worker, UUID.fromString(response.fileID), 
                                                response.content.data);}).run();

        //this is a response from a single worker, we will check for all 3 responses later
        Holder responder = new Holder(worker.id, UUID.fromString(response.fileID));

        //TODO: change to find the downloader of the main block corresponding to the sub block we just downloaded
        //      use the id it's containing to check the status of the 3 sub blocks
        //      requestedBlock is a holder object which is both workerID/uuid is the worker that the queen expects to be storing the requested block
        //      worker object with worker.id is the one who asked for the block
        //
        //TODO: add a place to store the blocks when we receive them.
        //      this will be a has table of (worker, BlockContainer)
        Worker downloaderA = downloadRequestsA.get(responder);
        Worker downloaderB = downloadRequestsB.get(responder);
        Worker downloaderC = downloadRequestsC.get(responder);

        //check to see which sub block was returned
        if(downloaderA != null){
            //use downloader to check the downloadRecord hash map and see if
            //more than 1 response has been received
            recordLock.lock();
            try {
                System.out.println("received response from A");
                //try to read the record table
                BlockContainer container = downloadRecord.get(downloaderA);
                //if the record doesn't exist, then make it
                if(container == null){
                    container = new BlockContainer();
                }

                //then add the new data to the record
                int len = Arrays.toString(response.content.data).length();
                container.A = Arrays.toString(response.content.data).substring(1, len - 1).split(",");
                downloadRecord.put(downloaderA, container);

                //figure out if it's possible to reconstruct the data
                if((container.B != null || container.C != null)){
                    //we have enough data to reconstruct
                    String[] recData = reconstruct(container);
                        
                    //determine if the block was odd or even, if true, remove the last byte from the array
                    if(db.blocks.getOdness(UUID.fromString(response.fileID))){
                        String[] t = new String[recData.length - 1]; 
                        for(int i = 0; i < t.length; i++){
                            t[i] = recData[i];
                        }
                        recData = t;
                    }

                    //convert back to byte array
                    byte[] sendData = new byte[recData.length];
                    for(int i = 0; i < sendData.length; i++){
                        sendData[i] = (byte)Integer.parseInt(recData[i].trim());
                    }

                    nm.sendToWorker(gson.toJson(new DownloadResponseMessage(sendData)), downloaderA);
                    downloadRecord.remove(downloaderA);
                    downloadRequestsA.remove(responder);
                }
            } finally {
                recordLock.unlock();
            }
        }
        else if (downloaderB != null){
            recordLock.lock();
            try{
                System.out.println("received response from B");
                BlockContainer container = downloadRecord.get(downloaderB);
                //if the record doesn't exist, then make it
                if(container == null){
                    container = new BlockContainer();
                }

                //then add the new data to the record
                int len = Arrays.toString(response.content.data).length();
                container.B = Arrays.toString(response.content.data).substring(1, len - 1).split(",");
                downloadRecord.put(downloaderB, container);

                //figure out if it's possible to reconstruct the data
                if((container.A != null || container.C != null)){
                    //we have enough data to reconstruct
                    String[] recData = reconstruct(container);

                    //determine if the block was odd or even, if true, remove the last byte from the array
                    if(db.blocks.getOdness(UUID.fromString(response.fileID))){
                        String[] t = new String[recData.length - 1]; 
                        for(int i = 0; i < t.length; i++){
                            t[i] = recData[i];
                        }
                        recData = t;
                    }

                    //convert back to byte array
                    byte[] sendData = new byte[recData.length];
                    for(int i = 0; i < sendData.length; i++){
                        sendData[i] = (byte)Integer.parseInt(recData[i].trim());
                    }
                    nm.sendToWorker(gson.toJson(new DownloadResponseMessage(sendData)), downloaderB);
                    downloadRecord.remove(downloaderB);
                    downloadRequestsB.remove(responder);
                }
            }
            finally{
                recordLock.unlock();
            }
            
        }
        else if (downloaderC != null){
            recordLock.lock();
            try{
                System.out.println("received response from C");
                BlockContainer container = downloadRecord.get(downloaderC);
                //if the record doesn't exist, then make it
                if(container == null){
                    container = new BlockContainer();
                }

                //then add the new data to the record
                int len = Arrays.toString(response.content.data).length();
                container.C = Arrays.toString(response.content.data).substring(1, len - 1).split(",");
                downloadRecord.put(downloaderC, container);

                //figure out if it's possible to reconstruct the data
                if((container.A != null || container.B != null)){
                    String[] recData = reconstruct(container);
                    //check the record to see if it was odd or not
                    //determine if the block was odd or even, if true, remove the last byte from the array
                    if(db.blocks.getOdness(UUID.fromString(response.fileID))){
                        String[] t = new String[recData.length - 1]; 
                        for(int i = 0; i < t.length; i++){
                            t[i] = recData[i];
                        }
                        recData = t;
                    }

                    //convert back to byte array
                    byte[] sendData = new byte[recData.length];
                    for(int i = 0; i < sendData.length; i++){
                        sendData[i] = (byte)Integer.parseInt(recData[i].trim());
                    }
                    nm.sendToWorker(gson.toJson(new DownloadResponseMessage(sendData)), downloaderC);
                    downloadRecord.remove(downloaderC);
                    downloadRequestsC.remove(responder);
                }

            }
            finally{
                recordLock.unlock();
            }

        }
        else{
            //not sure about this
            //if the downloader is null, ensure the record is removed from the 3 hash tables so we don't get memory leak
            downloadRecord.remove(worker);
            //downloadRecordA.remove(worker);
            //downloadRecordB.remove(worker);
            //downloadRecordC.remove(worker);
        }

        //is this always called
        // TODO redistribute to another worker if necessary.
        // TODO: if redistTarger != null then get the other two blocks and reconstruct the 3rd
        Holder redistTarget = redistTable.get(responder);
        if (redistTarget != null) {
          nm.sendToWorker(gson.toJson(new StoreFileMessage(UUID.fromString(response.fileID), redistTarget.blockID, response.content.data)), new Worker(redistTarget.workerID));
          redistTable.remove(responder);
        }
        break;
      case "error":
        ErrorMessage err = gson.fromJson(msg, ErrorMessage.class);

        if (err.isValid())
          System.out.println("Error from worker: " + msg);
        break;
      default:
        nm.sendToWorker(
            gson.toJson(new ErrorMessage("Unknown message type: " + messageType)), worker);
    }
  }

  protected String getMessageType(String msg) throws IllegalStateException {
    JsonElement message = JsonParser.parseString(msg);
    JsonObject messageObject = message.getAsJsonObject();
    return messageObject.get("type").getAsString();
  }

  //takes an array and a set of comma seperated numbers and puts half of the bytes into arrA,
  //and half in arrB
  //returns true if successful, false otherwise
  private String[][] splitBlock(String[] content){

      String[] bytes = new String[content.length];
      //parse everything into the byte array
      for(int i = 0; i < content.length; i++){
          bytes[i] = content[i];
      }

      //split the byte array into two halves
      String[] arrA = new String[bytes.length/2];
      String[] arrB = new String[bytes.length/2];

      System.out.println("content length");
      System.out.println(content.length);
      System.out.println("sub length");
      System.out.println(arrA.length);
      System.out.println(arrB.length);

      for(int i = 0; i < bytes.length; i++){
          if(i < bytes.length /2){
              arrA[i] = bytes[i];
          }
          else{
              arrB[i - arrB.length] = bytes[i];
          }
      }
      //System.out.println("arrA");
      //System.out.println(Arrays.toString(arrA));
      //System.out.println("arrB");
      //System.out.println(Arrays.toString(arrB));

      String[][] value = new String[2][arrA.length];
      value[0] = arrA;
      value[1] = arrB;
      return value;
  }

  //takes a string and turns it into an array of bytes
  private byte[] makeBytes(String s){
      String[] temp = s.split(",");
      byte[] b = new byte[temp.length];
      for(int i = 0; i < temp.length; i++){
          b[i] = (byte)Integer.parseInt(temp[i]);
      }
      return b;
  }

  //takes 3 byte arrays then uses A and B to generate a parity byte usin xor operation. places the result in C
  //returns true if successful
  private String[] genParity(String[] arrA, String[] arrB){
      //simple perform xor on the byte, WARNING this may need to be changed to be bitwise intstead of bytewise
      String[] arrC = new String[arrA.length];
      //arrC = new byte[arrA.length];
      for(int i = 0; i < arrA.length; i++){
          arrC[i] = Integer.toString((Integer.parseInt(arrA[i]) ^ Integer.parseInt(arrB[i])));
      }
      return arrC;
  }

  //takes the original message and a byte array and generates a new json string whith the same message data
  //but with the new byte array
  private String genMessage(String msg, String[] data){
      
      //System.out.println("genMessage, constructing from msg: " + msg);

      StringBuilder builder = new StringBuilder();
      builder.append("{type:upload,content:{type:Buffer,data:[");
      for(int i = 0; i < data.length; i++){
          //System.out.println("appending data");
          //System.out.println(data[i]);
          builder.append(data[i]);
          builder.append(",");
      }
      builder.deleteCharAt(builder.length() - 1);

      builder.append("]}}");

      //System.out.println("new message");
      ////System.out.println(builder.toString());
      return builder.toString();
  }

  //this will need to be a set of bytes!!!!!
  //helper that takes a Block container and returns the retuilt data
  private String[] reconstruct(BlockContainer container){
      //TODO: figure out what the string arrays look like when trying to reconstruct ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      System.out.println("Reconstruct, printing the 3 arrays");
      //System.out.println(Arrays.toString(container.A));
      //System.out.println(Arrays.toString(container.B));
//      System.out.println(Arrays.toString(container.C));
      //System.out.println(" ");


      String[] A = container.A;
      String[] B = container.B;
      String[] C = container.C;

      String[] data = null; 

      //A is missing, use C and B to reconstruct A
      if(A == null){
          A = new String[C.length];
          for(int i = 0; i < C.length; i++){
            A[i] = Integer.toString(Integer.parseInt(B[i].trim()) ^ Integer.parseInt(C[i].trim()));
          }
      }
      //b is missing, use C and A to reconstruct B
      else if(B == null){
          B = new String[C.length];
          for(int i = 0; i < C.length; i++){
              B[i] = Integer.toString(Integer.parseInt(A[i].trim()) ^ Integer.parseInt(C[i].trim()));
          }
      }
      //now both A and B should exist
      //use the A and B to make data
      data = new String[A.length * 2];
        for(int i = 0; i < data.length; i++){
            if(i < data.length/2){
                data[i] = A[i];
            }
            else{
                data[i] = B[i-data.length/2];
            }
        }

      return data;
  }

  public void handleUnregisteredMessage(String msg, Socket sender) {
    Gson gson = new Gson();

    String messageType;
    try {
      messageType = getMessageType(msg);
    } catch (JsonSyntaxException e) {
      nm.sendToSocket(gson.toJson(new ErrorMessage("Malformed message")), sender);
      return;
    }

    switch (messageType) {
      case "join":
        JoinMessage joinMessage = gson.fromJson(msg, JoinMessage.class);

        if (!joinMessage.isValid()) {
          nm.sendToSocket(gson.toJson(new ErrorMessage("Invalid join message.")), sender);
          break;
        }

        // TODO exception handling could be better here, but it is sufficient for now.
        Worker newWorker = null;
        try {
          newWorker = cm.newWorker(joinMessage.donation);
          nm.bindWorker(newWorker, sender);

          nm.sendToWorker(gson.toJson(new AcceptWorkerMessage(Integer.toString(newWorker.id))),
                                      newWorker);
        } catch (Exception e) {
          e.printStackTrace();
          nm.sendToSocket(gson.toJson(new ErrorMessage("Failed to add worker.")), sender);
        }
        
        // add uuids to database.
        for (String id: joinMessage.uuids) {
          try {
            UUID blockID = UUID.fromString(id);
            //why is it integer to string? I tried to fix this but not sure what you are going for
            //-aaron
            db.replaceableLocations.addEmptyBlock(newWorker, blockID); 
          } catch (IllegalArgumentException e) {
            nm.sendToSocket(gson.toJson(new ErrorMessage("Bad uuid.")), sender);
            break;
          }
        }


        break;
      case "connect":
        ConnectMessage connectMessage = gson.fromJson(msg, ConnectMessage.class);

        if (!connectMessage.isValid()) {
          nm.sendToSocket(gson.toJson(new ErrorMessage("ID is not an integer.")), sender);
          break;
        }
        if(db.workers.isBanned(new Worker(Integer.parseInt(connectMessage.ID)))) {
          nm.sendToSocket(gson.toJson(new ErrorMessage("Banned for bad reliability.")), sender);
          break;
        }

        Worker reconnectedWorker = new Worker(Integer.parseInt(connectMessage.ID));
        nm.bindWorker(reconnectedWorker, sender);
        nm.sendToWorker(gson.toJson(new OkMessage()), reconnectedWorker);
        break;
      case "upload":
      case "download":
      case "delete":
        nm.sendToSocket(gson.toJson(new ErrorMessage("You need to register/connect first.")), 
                        sender);
        break;
      default:
        nm.sendToSocket(
            gson.toJson(new ErrorMessage("Unknown message type: " + messageType)), sender);
    }
  }

  /**
   * Returns true if worker currently has a tcp connection to the server.
   */
  public boolean isConnected(Worker worker) {
    return nm.isConnected(worker);
  }

  public void sendValidationRequest(Worker worker, UUID blockID) {
    nm.sendToWorker((new Gson()).toJson(new FileRequestMessage(blockID)), worker);

    ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    scheduler.schedule(() -> {cm.validationFailure(worker, blockID);}, RESPONSE_TIMEOUT, TimeUnit.MILLISECONDS);
    validateTimeouts.put(new Holder(worker.id, blockID), scheduler); 
  }

  public void sendWarning(Worker worker) {
    nm.sendToWorker((new Gson()).toJson(new ErrorMessage("Validation check failed.")), worker);
  }

  public boolean redistBlock(DataBlock b) {
      //at this point, DataBlock represents a single sub block. 
      //    we should be able to use uuid uniqueness to find the whole record
      //TODO: update this to reconstruct, then redistribute
      //perform query to find the three sub blocks and main block id
      //request the two remaining blocks

    Gson gson = new Gson();

    ArrayList<Worker> holders = b.getAllHolders();
    ArrayList<Worker> onlineHolders = new ArrayList<Worker>();
    for(Worker holder : holders) {
      if (nm.isConnected(holder))
        onlineHolders.add(holder);
    }

    if(onlineHolders.size() == 0) {
      System.out.println("Unable to redistribute block.");
      return false;
    }

    Worker uploader = onlineHolders.get(0);

    //finds all of the holders of that block
    ArrayList<Worker> excludes = b.getAllHolders();
        
    HeldBlock storage = db.replaceableLocations.getWorkerWithReplaceableBlock(excludes);
    while (!nm.isConnected(new Worker(storage.HolderID))) {
      storage = db.replaceableLocations.getWorkerWithReplaceableBlock(excludes);
    }

    //find a way to request 2 sub blocks and reconstruct the 3rd, then send it to a new worker
    //key is the person we're getting the file from, value is the place we are storing it to
    redistTable.put(new Holder(uploader.id, b.id), new Holder(storage.HolderID, storage.blockID));
    nm.sendToWorker(gson.toJson(new FileRequestMessage(b.id)), uploader);
        
    return true;
  }

  public void disconnectWorker(Worker w) {
    nm.disconnectWorker(w); 
  }

  
  private class Holder {
    public int workerID;
    public UUID blockID;

    public Holder(int wID, UUID bID) {
      workerID = wID;
      blockID = bID;
    }

    @Override
    public int hashCode() {
      return blockID.hashCode() + workerID;
    }

    @Override
    public boolean equals(Object o) {
      if (o instanceof Holder) {
        Holder other = (Holder) o;
        return other.blockID.equals(blockID) && other.workerID == workerID;
      }
      return false;
    }
  }

  // Private classes for JSON serialization.

  private class AcceptWorkerMessage {
    private final String type = "acceptWorker";
    private final String workerID;

    public AcceptWorkerMessage(String id) {
      workerID = id;
    }
  }

  private class BufferJson {
    public String type = "Buffer";
    public byte[] data;

    public BufferJson(byte[] data) {
      this.data = data;
    }

    public boolean isValid() {
      return data != null;
    }
  }

  private class ConnectMessage {
    public String type;
    public String ID;

    public boolean isValid() {
      return ID != null && parsableAsInt(ID);  
    }
  }

  private class DeleteMessage {
    public String type;
    public String[] uuids;
  }

  private class DownloadMessage {
    public String type = "download";
    public String fileID;

    public DownloadMessage(String fileID) {
      this.fileID = fileID;
    }

    public boolean isValid() {
      return fileID != null && parsableAsUUID(fileID);
    }
  }

  private class DownloadResponseMessage {
    public final String type = "downloadResponse";
    public BufferJson contents;

    public DownloadResponseMessage(byte[] contents) {
      this.contents = new BufferJson(contents);
    }
  }

  private class ErrorMessage {
    public String type = "error";
    public String msg;

    public ErrorMessage(String msg) {
      this.msg = msg;
    }

    public boolean isValid() {
      return msg != null;
    }
  }

  private class FileAcceptMessage {
    public String type;
    public String fileID;
    
    public boolean isValid() {
      return fileID != null && parsableAsUUID(fileID);
    }
  }

  private class FileResponseMessage {
    public String type;
    public String fileID;
    public BufferJson content;

    public boolean isValid() {
      return content != null && content.isValid() && parsableAsUUID(fileID);
    }
  }

  private class FileRequestMessage {
    public final String type = "fileRequest";
    public final String fileID;

    public FileRequestMessage(UUID blockID) {
      fileID = blockID.toString();
    }
  }

  private class JoinMessage {
    public String type;
    public int donation = 0;
    public String[] uuids;

    // Doesn't check that uuids are all valid.
    public boolean isValid() {
      return donation != 0;
    }
  }

  private class OkMessage {
    private final String type = "ok";
  }

  private class OverwriteMessage {
    private final String type = "overwrite";
    private final String fileID;
    private final String blockID;

    public OverwriteMessage(String fileID, String blockID) {
      this.fileID = fileID;
      this.blockID = blockID;
    }
  }

  private class StoreFileMessage {
    public final String type = "storeFile";
    public final String fileID;
    public final String overwriteID;
    public final BufferJson contents;

    public StoreFileMessage(UUID ID, UUID overwriteID, byte[] contents) {
      this.fileID = ID.toString();
      this.overwriteID = overwriteID.toString();
      this.contents = new BufferJson(contents);
    }
  }

  private class UploadMessage {
    public String type = "upload";
    public BufferJson content;

    public boolean isValid() {
      return content != null && content.isValid();
    }
  }

  private class UploadAcceptMessage {
    public final String type = "uploadAccept";
    public final String uuid;

    public UploadAcceptMessage(UUID blockID) {
      this.uuid = blockID.toString();
    }
  }

  private class BlockContainer{
      public String[] A;
      public String[] B;
      public String[] C;
  }

  // Helper methods for message validation

  /**
   * Returns true if s can be parsed as an int.
   */
  private static boolean parsableAsInt(String s) {
    try {
      Integer.parseInt(s);
      return true;
    } catch (NumberFormatException e) {
      return false;
    }
  }

  /**
   * Returns true if s can be parsed as a UUID.
   */
  private static boolean parsableAsUUID(String s) {
    try {
      UUID.fromString(s);
      return true;
    } catch (IllegalArgumentException e) {
      return false;
    } 
  }
}
