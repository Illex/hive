package database;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;
import cluster.Worker;

/**
 * every method in this class (except the getBlock) is a basic wrapper around
 * an sql query. only simple logic done here, no checks.
 * @author u0985881
 *
 */
public class DatabaseManager {
  public static final String SQL_CONN = "jdbc:sqlite:hive-queen.db";
  public static final int SQL_QUERY_TIMEOUT_SEC = 5;
  public static final int BLOCK_SIZE = 4096;
  static int replicationFactor;
  public static byte[] SHAOFZEROS;
  public blocks blocks;
  public workers workers;
  public blocklocations blockLocations;
  public replaceablelocations replaceableLocations;
  Connection connection = null;

  public DatabaseManager(int replicationFactor) {
    try {
      
      connection = DriverManager.getConnection(SQL_CONN);
      this.replicationFactor = replicationFactor;

    } catch (SQLException e) {
      System.err.println("Failed to connect to the database!!");
      e.printStackTrace();
    }

    try {
      // make sure the tables exist
      PreparedStatement w = connection.prepareStatement("SELECT * FROM workers LIMIT 1");
      w.setQueryTimeout(SQL_QUERY_TIMEOUT_SEC);
      w.executeQuery();

      PreparedStatement h = connection.prepareStatement("SELECT * FROM holders LIMIT 1");
      h.setQueryTimeout(SQL_QUERY_TIMEOUT_SEC);
      h.executeQuery();

      PreparedStatement b = connection.prepareStatement("SELECT * FROM blocks LIMIT 1");
      b.setQueryTimeout(SQL_QUERY_TIMEOUT_SEC);
      b.executeQuery();
    } catch (SQLException e) {
      System.err.print("Failed to verify the database structure");
      e.printStackTrace();
    }
    
    blocks = new blocks();
    workers = new workers();
    blockLocations = new blocklocations();
    replaceableLocations = new replaceablelocations();


    byte[] zeros = new byte[BLOCK_SIZE];
    for(int i = 0; i < BLOCK_SIZE; i++) {
      zeros[i] = 0;
    }
    
    
    try {
      SHAOFZEROS = MessageDigest.getInstance("SHA-256").digest(zeros);
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
      
    }
  }
  
  public class blocks{
    //basic functions:
    //get existing block
    //add new block
    //delete block (move held instances to the replaceable db, remove from main db)
    //
    //
    
    
    /**
     * get an existing block out of the database
     * @param id
     * @return the block or null if not found
     */
    public DataBlock getBlock(UUID id) {

      Statement statement;
      try {

        statement = connection.createStatement();
        statement.setQueryTimeout(SQL_QUERY_TIMEOUT_SEC);
        ResultSet rs = statement.executeQuery("SELECT * FROM blocks WHERE BlockID = '" + id+"'");

        if (!rs.isBeforeFirst())
          return null; // return null if not found.

        byte[] sha = rs.getBytes("SHA256Hash");
        int ownerId = rs.getInt("OwnerWorkerID");

        return new DataBlock(DatabaseManager.this, id, sha, ownerId);


      } catch (SQLException e) {
        e.printStackTrace();
        System.err.println("Error while getting block of id: " + id.toString());
      }
      return null;
    }
    
    /**
     * add a block entry. no locations specified just register its existence
     * increments the internal blocks used counter too.
     * @param blockid
     * @param sha256
     * @param owner
     * @return if it added anything
     */
    public boolean addBlock(UUID blockid, byte[] sha256, Worker owner) {
      try {
        PreparedStatement statement =
            connection.prepareStatement("INSERT INTO blocks VALUES (?,?,?)");
        statement.setQueryTimeout(SQL_QUERY_TIMEOUT_SEC);
        statement.setString(1, blockid.toString());
        statement.setInt(2, owner.id);
        statement.setBytes(3, sha256);
        int changed=statement.executeUpdate();
        
        //only increment count if it worked
        if(changed == 1) {
          DatabaseManager.this.workers.incrementWorkerUsedBlocks(owner, changed);
          return true;
        }

      } catch (SQLException e) {
        e.printStackTrace();
        System.err.println("Error while adding block of id: " + blockid.toString());
      }
      return false;
    }

    //function to add a block id to the translation table
    public boolean addSubBlocks(UUID mainID, UUID AID, UUID BID, UUID CID, boolean f){
        try{
            PreparedStatement statement =
                connection.prepareStatement("INSERT INTO blockMap VALUES (?,?,?,?,?)");
            statement.setQueryTimeout(SQL_QUERY_TIMEOUT_SEC);
            statement.setString(1, mainID.toString());
            statement.setString(2, AID.toString());
            statement.setString(3, BID.toString());
            statement.setString(4, CID.toString());
            statement.setBoolean(5, f);
            int changed=statement.executeUpdate();

            if(changed == 1){
                return true;
            }
            else{return false;}

        } catch (SQLException e) {
            e.printStackTrace();
            System.err.println("Error while adding sub-block with record: " + mainID.toString() + ":" + AID.toString() + ":" + BID.toString() + ":" + CID.toString());
        }
        return false;
    }

    //TODO: add a get sub flag by mainID/subIDs
    public boolean getOdness(UUID u){
        //take advantage of the fact that uuids are unique
        try {
            //first get half random from holders
            PreparedStatement statement = connection.prepareStatement("SELECT Odd FROM blockMap WHERE BlockID = ? OR BlockA = ? OR BlockB = ? OR ParityBlock = ?");
            statement.setQueryTimeout(SQL_QUERY_TIMEOUT_SEC);
            statement.setString(1, u.toString());
            statement.setString(2, u.toString());
            statement.setString(3, u.toString());
            statement.setString(4, u.toString());
            ResultSet rs = statement.executeQuery();

            return rs.getBoolean(1);

        } catch (SQLException e) {
            e.printStackTrace();
            System.err.println("Error while getting oddness of id: " + u.toString());
            return false;
        }

    }

    //uses the main block id to return the 2 sub blocks and the parity block ids
    public UUID[] getSubBlocks(UUID mainID){
        Statement statement;
        try {

            statement = connection.createStatement();
            statement.setQueryTimeout(SQL_QUERY_TIMEOUT_SEC);
            ResultSet rs = statement.executeQuery("SELECT * FROM blockMap WHERE BlockID = '" + mainID.toString() +"'");

            if (!rs.isBeforeFirst())
            return null; // return null if not found.

            UUID[] temp = new UUID[3];
            temp[0] = UUID.fromString(rs.getString("BlockA"));
            temp[1] = UUID.fromString(rs.getString("BlockB"));
            temp[2] = UUID.fromString(rs.getString("ParityBlock"));

            return temp;
            //return new UUID[rs.BlockA,rs.BlockB,rs.ParityBlock];


        } catch (SQLException e) {
            e.printStackTrace();
            System.err.println("Error while getting sub blocks of block: " + mainID.toString());
        }
        return null;
    }

    public UUID[] getSubBlocksByParts(UUID AID, UUID BID, UUID CID){
        Statement statement;
        try {
            statement = connection.createStatement();
            statement.setQueryTimeout(SQL_QUERY_TIMEOUT_SEC);
            ResultSet rs = statement.executeQuery("SELECT * FROM blockMap WHERE BlockA = '" + AID.toString() + "and WHERE BlockB = " + BID.toString()+ "and WHERE ParityBlock = " + CID.toString() +"'");

            //dont know what this does
            if (!rs.isBeforeFirst())
            return null; // return null if not found.

            UUID[] temp = new UUID[3];
            temp[0] = UUID.fromString(rs.getString("BlockA"));
            temp[1] = UUID.fromString(rs.getString("BlockB"));
            temp[2] = UUID.fromString(rs.getString("ParityBlock"));
            return temp;
            //return new UUID[rs.BlockID, rs.BlockA, rs.BlockB, rs.ParityBlock];


        } catch (SQLException e) {
            e.printStackTrace();
            System.err.println("Error while getting sub block record in getSubBlocksByParts");
        }
        return null;
    }

    public boolean removeSubBlocks(UUID mainID){
        try {
            PreparedStatement statement =
                connection.prepareStatement("DELETE FROM blockMap WHERE BlockID= ?");
            statement.setQueryTimeout(SQL_QUERY_TIMEOUT_SEC);
            statement.setString(1, mainID.toString());
            int changed = statement.executeUpdate();
        
            //decrement count if worked
            if(changed == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.err.println("Error and potential desync while deleting block and sub-blocks of id: " + mainID.toString());
        }
        return false;
    }

    /**
     * 
     * @param blockid
     * @return
     */
    protected boolean deleteBlock(UUID blockid) {
      //formerly "forgetblock"
      
      Worker owner = new Worker(getBlock(blockid).ownerID);
      try {
        PreparedStatement statement =
            connection.prepareStatement("DELETE FROM blocks WHERE BlockID = ?");
        statement.setQueryTimeout(SQL_QUERY_TIMEOUT_SEC);
        statement.setString(1, blockid.toString());
        int changed = statement.executeUpdate();
        
        //decrement count if worked
        if(changed == 1) {
          DatabaseManager.this.workers.incrementWorkerUsedBlocks(owner, -changed);
          return true;
        }
      } catch (SQLException e) {
        e.printStackTrace();
        System.err.println("Error and potential desync while deleting block of id: " + blockid.toString());
      }
      return false;
    }

    
    
    /**
     * generates a random block in the database (in the "replaceable block" table),
     *  and returns its details..
     * @param dataOut the generated random data is put into this array. will be overwritten
     */
    public DataBlock generateGarbageBlock(byte[] dataOut) {
      if(dataOut.length != BLOCK_SIZE) {
        dataOut = new byte[BLOCK_SIZE];
      }
      //fill the array with data
      new Random().nextBytes(dataOut);
      
      byte[] hash;
      try {
        hash = MessageDigest.getInstance("SHA-256").digest(dataOut);
      } catch (NoSuchAlgorithmException e) {
        e.printStackTrace();
        return null;
      }
      
      DataBlock dblock = new DataBlock(DatabaseManager.this, UUID.randomUUID(), hash, -1);
      
      
      
      return dblock;
      
      
    }
    
    
  }
  public class workers{
    /**
     * adds a new worker in a fresh worker state.
     * @param blocksDonated
     * @return worker id
     */
    public int addWorker(int blocksDonated) {
      try {
        PreparedStatement insert =
            connection.prepareStatement("INSERT INTO workers(LocalBlocksDonated, LocalBlocksUsed, "
                + "EarnedBlocksTotal, EarnedBlocksUsed) VALUES (?,0,0,0);");
        PreparedStatement fetch =
            connection.prepareStatement("SELECT last_insert_rowid() FROM workers");
        insert.setQueryTimeout(SQL_QUERY_TIMEOUT_SEC);
        insert.setInt(1, blocksDonated);
        int changed = insert.executeUpdate();

        if (changed < 1) {
          throw new SQLException("Failed to insert worker.");
        }

        ResultSet rs = fetch.executeQuery();
        return rs.getInt(1);

      } catch (SQLException e) {
        e.printStackTrace();
        System.err.println("Error while adding worker of id");
      }
      return -1;
    }
    
    public int deleteWorker(Worker w) {
      try {
        PreparedStatement insert =
            connection.prepareStatement("DELETE FROM workers WHERE ID = ?;");
        insert.setQueryTimeout(SQL_QUERY_TIMEOUT_SEC);
        insert.setInt(1, w.id);
        int changed = insert.executeUpdate();

        if (changed < 1) {
          throw new SQLException("Failed to delete worker.");
        }

       

      } catch (SQLException e) {
        e.printStackTrace();
        System.err.println("Error while adding worker of id");
      }
      return -1;
    }
    public Iterable<Worker> getAllWorkers() {
      try {

        Statement statement = connection.createStatement();
        statement.setQueryTimeout(SQL_QUERY_TIMEOUT_SEC);
        ResultSet rs = statement
            .executeQuery("SELECT ID FROM Workers");
        
        ArrayList<Worker> workers = new ArrayList<Worker>();
       
        if(!rs.isClosed())
          workers.add(new Worker(rs.getInt(1)));
        while (rs.next())
          workers.add(new Worker(rs.getInt(1)));
        return workers;

      } catch (SQLException e) {
        
        e.printStackTrace();
        System.err.println("Error while getting free worker");
      }

      return null;
    }
    public int getWorkerDonatedBlocks(Worker w) {

      try {

        Statement statement = connection.createStatement();
        statement.setQueryTimeout(SQL_QUERY_TIMEOUT_SEC);
        ResultSet rs =
            statement.executeQuery("SELECT LocalBlocksDonated FROM Workers WHERE ID = " + w.id);
        return rs.getInt(1);

      } catch (SQLException e) {
        
        e.printStackTrace();
        System.err.println("Error while getting worker donated info for worker of id: " + w.id);
      }

      return -1;
    }

    public boolean setWorkerDonatedBlocks(Worker w, int newAmount) {

      try {

        Statement statement = connection.createStatement();
        statement.setQueryTimeout(SQL_QUERY_TIMEOUT_SEC);
        int changed = statement.executeUpdate(
            "UPDATE Workers SET LocalBlocksDonated = " + newAmount + " WHERE ID = " + w.id);
        return changed > 0;

      } catch (SQLException e) {
        
        e.printStackTrace();
        System.err.println("Error while getting worker donated info for worker of id: " + w.id);
      }

      return false;
    }

    public int getWorkerUsedBlocks(Worker w) {

      try {

        Statement statement = connection.createStatement();
        statement.setQueryTimeout(SQL_QUERY_TIMEOUT_SEC);
        ResultSet rs =
            statement.executeQuery("SELECT LocalBlocksUsed FROM Workers WHERE ID = " + w.id);
        return rs.getInt(1);

      } catch (SQLException e) {
        
        e.printStackTrace();
        System.err.println("Error while getting worker used blocks info for worker of id: " + w.id);
      }

      return -1;
    }

    public boolean incrementWorkerUsedBlocks(Worker w, int newAmount) {

      try {

        Statement statement = connection.createStatement();
        statement.setQueryTimeout(SQL_QUERY_TIMEOUT_SEC);
        int changed = statement
            .executeUpdate("UPDATE Workers SET LocalBlocksUsed = LocalBlocksUsed + " + newAmount + " WHERE ID = " + w.id);
        return changed > 0;

      } catch (SQLException e) {
        
        e.printStackTrace();
        System.err.println("Error while getting worker donated info for worker of id: " + w.id);
      }

      return false;
    }

    public double getWorkerBlockScore(Worker w) {
      try {
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(SQL_QUERY_TIMEOUT_SEC);
        ResultSet rs =
            statement.executeQuery("SELECT BlockValidationScore FROM Workers WHERE ID = " + w.id);
        return rs.getDouble(1);
      } catch (SQLException e) {
        
        e.printStackTrace();
        return 1;
      }
    }
    public void updateWorkerBlockScore(Worker w, boolean score) {
      // TODO Auto-generated method stub
      
      //get estimatedrtt
      double EstimatedRTT = getWorkerBlockScore(w);
      
      
      
      //calc the new one
      int SampleRTT = score ? 1 : 0;
      final double a = 0.001;  
      EstimatedRTT = (1 - a) * EstimatedRTT + a * SampleRTT;
     
      System.out.format("Validation score for worker %d updated to %f.\n", w.id, EstimatedRTT); 
      
      //store it
      try {

        Statement statement = connection.createStatement();
        statement.setQueryTimeout(SQL_QUERY_TIMEOUT_SEC);
        int changed = statement
            .executeUpdate("UPDATE Workers SET BlockValidationScore = " + EstimatedRTT + " WHERE ID = " + w.id);
        if(changed != 1)
          System.out.println("WARNING: query should have changed 1 row, changed "+changed);

      } catch (SQLException e) {
        e.printStackTrace();
      }
      
    }
    
    public double getWorkerUptimeScore(Worker w) {
      
      try {
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(SQL_QUERY_TIMEOUT_SEC);
        ResultSet rs =
            statement.executeQuery("SELECT UptimeScore FROM Workers WHERE ID = " + w.id);
        return rs.getDouble(1);
      } catch (SQLException e) {
        
        e.printStackTrace();
        return 1;
      }
    }
    
    /**
     * use the estimatedrtt algorithm to add another reading to the tracked score
     * @param w
     * @param score true if online
     */
    public void updateWorkerUptimeScore(Worker w, boolean score) {
      // TODO Auto-generated method stub
      
      
      //get estimatedrtt
      
      double EstimatedRTT = getWorkerUptimeScore(w);
      
      //calc the new one
      int SampleRTT = score ? 1 : 0;
      final double a = 0.001;  
      EstimatedRTT = (1 - a) * EstimatedRTT + a * SampleRTT;
      
      
      //store it
      try {

        Statement statement = connection.createStatement();
        statement.setQueryTimeout(SQL_QUERY_TIMEOUT_SEC);
        int changed = statement
            .executeUpdate("UPDATE Workers SET UptimeScore = " + EstimatedRTT + " WHERE ID = " + w.id);
        if(changed != 1)
          throw new SQLException("should have changed 1, changed "+changed);

      } catch (SQLException e) {
        e.printStackTrace();
      }
      
    }

    public void addWorkerToBannedList(Worker w) {
      //formerly "banworker"
      try {
        PreparedStatement insert = 
            connection.prepareStatement("INSERT INTO bannedworkers (ID) VALUES (?);");
        insert.setInt(1, w.id);
        insert.setQueryTimeout(SQL_QUERY_TIMEOUT_SEC);
        insert.executeUpdate();
        
        //TODO: move this logic:
        PreparedStatement statement = connection.prepareStatement("SELECT BlockID from blocks WHERE OwnerWorkerID = ?;");
        statement.setQueryTimeout(SQL_QUERY_TIMEOUT_SEC);
        statement.setInt(1, w.id);
        ResultSet rs = statement.executeQuery();
        
       
        if(!rs.isClosed()) {
          String id = rs.getString(1);
          if (id != null) {
            DatabaseManager.this.blocks.getBlock(UUID.fromString(id)).delete();
          }
          while (rs.next()) {
            id = rs.getString(1);
            DatabaseManager.this.blocks.getBlock(UUID.fromString(id)).delete();
          }
        }else {
          //had on blocks to delete
        }
        
        
        DatabaseManager.this.workers.deleteWorker(w);
        
        
        
        
        
      } catch (SQLException e) {
        
        e.printStackTrace();
        System.err.println("DB ERROR BANNING USER");
      }
      
     
  
    }
    
    public boolean isBanned(Worker w) {
      //formerly "banworker"
      try {
        PreparedStatement statement = connection.prepareStatement("SELECT count(*) from bannedworkers WHERE ID = ?;");
        statement.setQueryTimeout(SQL_QUERY_TIMEOUT_SEC);
        statement.setInt(1, w.id);
        ResultSet rs = statement.executeQuery();
         
        return rs.getInt(1) > 0;
        
      } catch (SQLException e) {
        
        e.printStackTrace();
        System.err.println("DB ERROR CHECKING BAN");
        return false;
      }
  
    }
  }
  public class blocklocations{
    //todo: "convert to replaceable"
    
    public boolean addBlockHolder(Worker holder, UUID blockid) {
      // used only for initialization of a worker, or for expanding the space.
      // adding a file will have us replacing the queens fake blocks.
      try {
        PreparedStatement statement = connection.prepareStatement("INSERT INTO holders VALUES (?,?,?)");
        statement.setQueryTimeout(SQL_QUERY_TIMEOUT_SEC);
        statement.setInt(1, holder.id);
        statement.setString(2, blockid.toString());
        statement.setInt(3, 1);
        return statement.executeUpdate() > 0;
      } catch (SQLException e) {
        e.printStackTrace();
        System.err.println("Error while adding block location of id: " + blockid.toString());
      }
      return false;
    }
    
    /**
     * use cases: retrieving a file/block, deleting a file/block
     * 
     * @return
     */
    protected ArrayList<Integer> getBlockLocations(UUID blockid) {
      Statement statement;
      ArrayList<Integer> toret = new ArrayList<Integer>();
      try {

        statement = connection.createStatement();
        statement.setQueryTimeout(SQL_QUERY_TIMEOUT_SEC);
        ResultSet rs = statement
            .executeQuery("SELECT HolderWorkerID FROM holders WHERE BlockID = '" + blockid.toString()+"'"); // non
                                                                                                       // prepared                                                                                     // object
        if(!rs.isClosed())
          toret.add(rs.getInt("HolderWorkerID"));
        while (rs.next()) {
          toret.add(rs.getInt("HolderWorkerID"));
        }

      } catch (SQLException e) {

        e.printStackTrace();
        System.err
            .println("Error while getting block locations for block of id: " + blockid.toString());
      }
      return toret;
    }


    /**
     * get a list of all the blocks a worker holds
     * @param w
     * @return
     */
    public ArrayList<DataBlock> getWorkerHeldBlocks(Worker w) {
      try {
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(SQL_QUERY_TIMEOUT_SEC);
        ResultSet rs =
            statement.executeQuery("SELECT BlockID FROM Holders WHERE HolderWorkerID = " + w.id);
      
        ArrayList<DataBlock> blocks = new ArrayList<DataBlock>();
        //blocks.add(new DataBlock(DatabaseManager.this,UUID.fromString(rs.getString(2)),new byte[256], 0));
        
        if(!rs.isClosed())
          blocks.add(DatabaseManager.this.blocks.getBlock(UUID.fromString(rs.getString(1))));
        while(rs.next())
        {
          blocks.add(DatabaseManager.this.blocks.getBlock(UUID.fromString(rs.getString(1))));
        }
        
        return blocks;
      } catch (SQLException e) {
        
        e.printStackTrace();
        return null;
      }
    }
    
    /**
     * 
     * @param i amount
     * @return an arraylist containing random block locations, half blocks in use and half garbage
     * or null if err
     */
    public ArrayList<HeldBlock> getStalestHeldBlocks(int i){
      //discussing with the guys random checking would be just fine
      //should be filled out correctly in the future??
      //tbh the chaotic randomness might be more secure than the predictability of 
      //actually going oldest
      try {
        ArrayList<HeldBlock> blocks = new ArrayList<HeldBlock>(i);
       
        //first get half random from holders
        PreparedStatement statement = connection.prepareStatement("SELECT * FROM holders ORDER BY RANDOM() LIMIT ?");
        statement.setQueryTimeout(SQL_QUERY_TIMEOUT_SEC);
        statement.setInt(1, i);
        ResultSet rs = statement.executeQuery();
        
        //rs.first();
        if(!rs.isClosed())
          blocks.add(new HeldBlock(rs.getInt(1),UUID.fromString(rs.getString(2))));
        while(rs.next())
        {
          blocks.add(new HeldBlock(rs.getInt(1),UUID.fromString(rs.getString(2))));
        }
        
        //then get the rest from garbage blocks
        statement = connection.prepareStatement("SELECT * FROM replaceableholderlocations ORDER BY RANDOM() LIMIT ?");
        statement.setQueryTimeout(SQL_QUERY_TIMEOUT_SEC);
        statement.setInt(1, i - blocks.size());//tries to make up for if theres not enough from the first query
        rs = statement.executeQuery();
        
        //rs.first();
        if(!rs.isClosed())
          blocks.add(new HeldBlock(rs.getInt(1),UUID.fromString(rs.getString(2))));
        while(rs.next())
        {
          //blocks.add(new HeldBlock(rs.getInt(1),UUID.fromString(rs.getString(2))));
        }
        
        if(blocks.size() != i) {
          System.out.println("warning: requested "+i+" random blocks, found "+blocks.size());
        }
        return blocks;
      }
      catch (SQLException e) {
        e.printStackTrace();
        
      }
      return null;
      
      
    }
    
    public void updateLastChecked(HeldBlock toValidate) {
      // TODO Auto-generated method stub
     //this does nothing while the above method is random.
      
    }
    
    /**
     * save that a worker is no longer storing (holding) an instance of a particular block.
     * 
     * @param blockid
     * @param holder
     * @return if it deleted a block
     */
    protected boolean removeBlockFromHolder(Worker holder, UUID blockid) {
      // TODO: overloads that take the worker id instead of the worker object?
      try {
        PreparedStatement statement = connection
            .prepareStatement("DELETE FROM holders WHERE BlockID = ? AND HolderWorkerID = ?");
        statement.setQueryTimeout(SQL_QUERY_TIMEOUT_SEC);
        statement.setString(1, blockid.toString());
        statement.setInt(2, holder.id);
        return statement.executeUpdate() > 0;
      } catch (SQLException e) {
        e.printStackTrace();
        System.err.println("Error while removing block from location of id: " + blockid.toString());
      }
      return false;
      
    }
    
    /**
     * 
     * @return amount of blocks on the network total
     */
    public int networkCapacity() {
      //formerly "heldblockcount"
      PreparedStatement s;
      
      try {
        s = connection.prepareStatement("SELECT COUNT(*) FROM holders");
        int count = s.executeQuery().getInt(1);
        
        s = connection.prepareStatement("SELECT COUNT(*) FROM replaceableholderlocations");
        count += s.executeQuery().getInt(1);
        return count;
      }catch(SQLException e) {
        e.printStackTrace();
        System.err.println("failure in networkCapacity");
      }
      return -1;
    }

  }
  
  public class replaceablelocations{
    
    public boolean addEmptyBlock(Worker holder, UUID blockid) {
      //empty blocks are just full of zeros.
      //we precompute the hash of a block of zeros in the constructor.
      
      return newReplaceableBlockLocation(holder,blockid,SHAOFZEROS);
    }
    
    /**
     * 
     * @param blockid
     * @return
     */
    public boolean deleteReplaceableBlock(UUID blockid, int workerID) {
      //formerly "forgetblock"
      try {
        PreparedStatement statement =
            connection.prepareStatement("DELETE FROM replaceableholderlocations WHERE BlockID = ? AND HolderWorkerID = ?");
        statement.setQueryTimeout(SQL_QUERY_TIMEOUT_SEC);
        statement.setString(1, blockid.toString());
        statement.setString(2, Integer.toString(workerID));
        int changed = statement.executeUpdate();
      } catch (SQLException e) {
        e.printStackTrace();
        System.err.println("Error and potential desync while deleting block of id: " + blockid.toString());
      }
      return false;
    }
    
    public boolean newReplaceableBlockLocation(Worker holder, UUID blockid, byte[] sha256) {
      // used only for initialization of a worker, or for expanding the space.
      // adding a file will have us replacing the queens fake blocks.
      try {
        PreparedStatement statement = connection.prepareStatement("INSERT INTO replaceableholderlocations VALUES (?,?,?)");
        statement.setQueryTimeout(SQL_QUERY_TIMEOUT_SEC);
        statement.setInt(1, holder.id);
        statement.setString(2, blockid.toString());
        statement.setBytes(3, sha256);
        return statement.executeUpdate() > 0;
      } catch (SQLException e) {
        e.printStackTrace();
        System.err.println("Error while adding block location of id: " + blockid.toString());
      }
      return false;
    }
    
    
    public HeldBlock getWorkerWithReplaceableBlock() {
      
      //just run the normal one and dont exclude any
      ArrayList<Worker> nobody = new ArrayList<Worker>();
      nobody.add(new Worker(-1));
      
      return getWorkerWithReplaceableBlock(nobody );
      
    }
    
    
    /**
     * gets a free slot in the network thats not in one of the excluded workers.
     * @param excludeList don't return a heldblock on any of these workers.
     * @return the heldblock found, or null on error or if one isnt found.
     */
    public HeldBlock getWorkerWithReplaceableBlock(ArrayList<Worker> excludeList ) {
      //formerly getWorkerWithFreeBlock
      try {

        Statement statement = connection.createStatement();
        statement.setQueryTimeout(SQL_QUERY_TIMEOUT_SEC);
        
       /* //due to db limitations the best way to do this is to make the exclude list a fixed
        //length. this means we err if there are too many in the exclude list.
        int maxLength = 10; //equal to the amount of question marks in the query
        if (excludeList.length > maxLength)
          throw new SQLException("getWorkerWithReplaceableBlock: too many excludelist members.");
        
        for(int i = 1; i < excludeList.length + 1; i++) {
          statement.setInt(i,excludeList[i-1].id);
        }*/
        
        String in = "";
        for(Worker w : excludeList) {
          in += ","+w.id;
        }
        in = in.substring(1);//remove leading comma
        
        ResultSet rs = statement
            .executeQuery("SELECT HolderWorkerID,BlockID FROM replaceableholderlocations WHERE holderworkerid NOT IN ("+in+") ORDER BY RANDOM() LIMIT 1");

        
       
        return new HeldBlock(rs.getInt(1),UUID.fromString(rs.getString(2)));

      } catch (SQLException e) {
        e.printStackTrace();
        System.err.println("Error while getting free worker");
      }

      return null;
    }
  }
  public void CloseConnection() {
    try {
      connection.close();
    } catch (SQLException e) {
      System.err.println("WARNING: failed to close the database conn");
      e.printStackTrace();
    }
  }

}
