-- A table that is supposed to sit in fron of the blocks and holders table
-- it maps a single block id to 3 sub-block ids to enable the raid mapping
CREATE TABLE blockMap (
                    BlockID Text(36) UNIQUE, 
                    BlockA Text(36) UNIQUE, 
                    BlockB Text(36) UNIQUE, 
                    ParityBlock Text(36) UNIQUE, 
                    Odd BOOlEAN,

                    PRIMARY KEY (BlockID)
                    );

CREATE TABLE blocks (
					 BlockID Text(36) UNIQUE, 
                     OwnerWorkerID INTEGER,
                     SHA256Hash blob(256),
                     
                     PRIMARY KEY (BlockID)
                    );
                    
CREATE TABLE holders(
					-- a  list linking real blocks to workers who are storing them
					 HolderWorkerID INTEGER,
                     BlockID Text(36),
                     IsUpToDate Integer, 
                     
                     FOREIGN KEY(BlockID) REFERENCES blocks(BlockID)
                    );
                    
                    
CREATE TABLE replaceableholderlocations(
					-- A list of 'empty blocks' (ethereal?). (Consisting of
					-- 'garbage blocks' and blocks that were 'deleted')
					-- we keep the hash of the fake and dead blocks so we can
					-- verify them every once in a while to make sure people are storing.
					-- we key by holder 
					HolderWorkerID INTEGER,
					BlockID Text(36),
					SHA256Hash blob(256),
					
					
					-- each worker should only ever hold one copy of a given block (duh)
					PRIMARY KEY (BlockID, HolderWorkerID)								
);
                    
CREATE TABLE workers(
					ID INTEGER PRIMARY KEY AUTOINCREMENT,
					LocalBlocksDonated INTEGER,
					LocalBlocksUsed INTEGER,
					
					EarnedBlocksTotal INTEGER,
					EarnedBlocksUsed INTEGER,
					
					BlockValidationScore REAL DEFAULT 1,
					UptimeScore REAL DEFAULT 1
);

CREATE TABLE bannedworkers(
	ID INTEGER PRIMARY KEY
);
     
CREATE INDEX WorkerWithEmptyBlock on replaceableholderlocations(HolderWorkerID);
     
CREATE INDEX HolderWorkerIndex on holders(HolderWorkerID);

CREATE INDEX HolderBlockIndex on holders(BlockID);



                     
