package database;

import java.util.UUID;

public class HeldBlock {
  public HeldBlock(int HolderID, UUID blockID) {
    this.blockID = blockID;
    this.HolderID = HolderID;
  }
  public int HolderID;
  public UUID blockID;
}
