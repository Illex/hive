package database;

import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;
import cluster.Worker;
import cluster.ClusterManager;


/**
 * representative of an piece of data not attatched to any location its in.
 * 
 * @author u0985881
 *
 */
public class DataBlock {

  DatabaseManager dbm;
  private boolean deleted;
  public final UUID id;
  public final int ownerID;
  public final byte[] sha;
  // this is set so that once you delete the block you cant use it any more

  // for use by the databasemanager to construct a new block.
  protected DataBlock(DatabaseManager db, UUID id, byte[] sha256, int ownerID) {
    dbm = db;
    this.id = id;
    this.sha = sha256;
    this.ownerID = ownerID;
  }



  /**
   * 
   * @return ids of workers holding this block.
   */
  public ArrayList<Integer> getHoldersIds() {
    return dbm.blockLocations.getBlockLocations(id);
  }

  public ArrayList<Worker> getAllHolders() {
    ArrayList<Worker> toret = new ArrayList<Worker>();
    for (Integer d : getHoldersIds()) {
      toret.add(new Worker(d));
    }

    return toret;
  }

  public Worker getRandomHolder() {
    ArrayList<Integer> ids = getHoldersIds();
    if(ids.size() == 0) return null;
    return new Worker(ids.get(new Random().nextInt(ids.size())));
    
  }
  /*
   * public ArrayList<Worker> getLiveHolders() { ArrayList<Worker> workers = new
   * ArrayList<Worker>(); for(Integer w : getHoldersIds()) { workers.add() } //
   * database.findlocatoins return null; }
   */


  public void addHolder(Worker worker) {
    dbm.blockLocations.addBlockHolder(worker, id);
    return;
  }

  public void removeHolder(Worker holder) {
    dbm.blockLocations.removeBlockFromHolder(holder, id);
    return;
  }



  // if has no locations
  public boolean canDelete() {
    return this.getHoldersIds().size() == 0;
  }
  
  /**
   * deletes a block by converting its held locations to replaceable garbage blocks,
   * then forgetting the block's metadata
   * @return
   */

  public boolean delete() {
    //flow: 
    //1a. turn each location of this block
    //  into a replaceable location
    //1b. remove each location from blocks/holders association
    //2. delete the blocks from the blocks list
    
    //1: make my locations replaceable
    
    //1a. register replaceable locations overlapping with this blocks' slots
    for(Worker w : getAllHolders()) {
      dbm.replaceableLocations.newReplaceableBlockLocation(w, this.id, this.sha);
    }
    
    //1b. delete my locations from the db
    for(Integer i : getHoldersIds()) {
      //dbm.BlockLocations.removeBlockFromHolder(new Worker(i), id);
      removeHolder(new Worker(i));
    }
    
    
    //2. 
    if (canDelete()) {
      boolean success = dbm.blocks.deleteBlock(this.id);
      this.deleted = true;
      return success;
    }
    // DISCUSS: throw exception if cant delete?
    // throw new Exception("Can not delete block with data out there.");
    return false;



  }



}
