import controller.Controller;
import network.NetworkManager;
import java.io.IOException;
import org.apache.commons.cli.*;

public class Queen {
	final static int PORT = 2113;
	static int REPLICATION_FACTOR = 3; //how many copies of any block should exist 
	
    // Time between uptime checks in seconds.
    private static int uptimeCheckInterval = 15 * 60;

    // Every block should be validated about this often in seconds.
    private static int validationRoundTime = 60 * 60 * 24 * 7;
	
	public static void main (String[] args) throws ParseException, IOException {
		System.out.println("hello");
	
        Options clOptions = new Options();

        clOptions.addOption(new Option("u", true, "Time between uptime checks in seconds."));
        clOptions.addOption(new Option("v", true, "Approximate time between checks per block in seconds."));

        CommandLineParser parser = new DefaultParser();

        CommandLine parsedArgs = parser.parse(clOptions, args);

        uptimeCheckInterval = Integer.parseInt(parsedArgs.getOptionValue("u", Integer.toString(uptimeCheckInterval)));
        validationRoundTime = Integer.parseInt(parsedArgs.getOptionValue("v", Integer.toString(validationRoundTime)));

		
		Controller c = new Controller(PORT, REPLICATION_FACTOR, uptimeCheckInterval, validationRoundTime);
		c.begin();
		
		
	}
}
