package pseudoworker;

import java.net.UnknownHostException;
import java.io.IOException;

public class DefectiveWorker extends PseudoWorker {

  public DefectiveWorker(String address, int port, int donation) throws IOException, UnknownHostException {
    super(address, port, donation);
  }

  @Override
  public Integer apply(String msg) {
    // If we get a file request ignore it, otherwise call the super method.
    if (getMessageType(msg).equals("fileRequest"))
      return 0;
    return super.apply(msg);
  }

  @Override
  protected void printDebug(String msg) {
    System.out.format("[ID %s](defective): %s\n", id, msg);
  }
}
