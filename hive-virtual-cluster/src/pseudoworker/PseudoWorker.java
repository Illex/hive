package pseudoworker; // TODO decide what this should be.

import java.util.UUID;
import java.util.HashSet;
import java.util.function.Function;
import java.util.Collection;
import java.util.Arrays;
import java.util.List;
import networker.Networker;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

public class PseudoWorker implements Function<String, Integer> {
  private final static int BYTES_PER_BLOCK = 1000000; // ~1 MB

  private Networker net;

  private HashSet<UUID> empty;

  public HashSet<UUID> storedFiles;

  public String id = "";

  public boolean banned = false;

  private Function<UUID, Integer> onUploadAccept;

  private Function<ByteBuffer, Integer> onDownload;

  public PseudoWorker(String address, int port, int donation) throws UnknownHostException, IOException {
    net = new Networker(this, address, port);
    
    // Generate empty uuids

    empty = new HashSet<UUID>();
    for (int i = 0; i < donation; i++) {
      empty.add(UUID.randomUUID());
    }

    storedFiles = new HashSet<UUID>();

    // Send connection message
    net.send(new Gson().toJson(new JoinMessage(donation, empty))); 

    // Start listening
    new Thread(() -> {
      try {
        net.listen();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }).start();
  }

  protected String getMessageType(String msg) throws IllegalStateException {
    JsonElement message = JsonParser.parseString(msg);
    JsonObject messageObject = message.getAsJsonObject();
    return messageObject.get("type").getAsString();
  }

  private String getPath(UUID uuid) {
    assert(!id.equals(""));

    return String.format("blocks/%s/%s", id, uuid);
  }

  @Override
  public Integer apply(String msg) {
    String type = getMessageType(msg);

    Gson gson = new Gson();
    
    if (id.equals("")) {
      assert(type.equals("acceptWorker"));

      AcceptWorkerMessage acceptMsg = gson.fromJson(msg, AcceptWorkerMessage.class);
      id = acceptMsg.workerID;
      printDebug("Connected successfully.");
      new File("blocks/" + id).mkdir();
      return 0;
    }

    try {
      switch (type) {
        case "fileRequest":
          FileRequestMessage fileRequest = gson.fromJson(msg, FileRequestMessage.class);

          printDebug(String.format("Asked to retrieve block: %s.", fileRequest.fileID)); 

          FileInputStream input = null;
          try {
            input = new FileInputStream(new File(getPath(UUID.fromString(fileRequest.fileID))));
          } catch (java.io.FileNotFoundException e) {
            e.printStackTrace();
          } 

          byte[] buf = new byte[BYTES_PER_BLOCK];

          int length = input.read(buf);
          input.close();

          net.send(gson.toJson(new FileResponseMessage(UUID.fromString(fileRequest.fileID), Arrays.copyOfRange(buf, 0, length))));
          break;
        case "downloadResponse":
          DownloadResponseMessage downloadResponse = gson.fromJson(msg, DownloadResponseMessage.class);

          printDebug("Received download response.");
          
          if (onDownload != null)
            onDownload.apply(ByteBuffer.wrap(downloadResponse.contents.data));
          break;
        case "storeFile":
          StoreFileMessage storeMsg = gson.fromJson(msg, StoreFileMessage.class);

          printDebug(String.format("Asked to overwrite block: %s with block: %s.", storeMsg.overwriteID, storeMsg.fileID));

          if (empty.contains(storeMsg.overwriteID))
            empty.remove(storeMsg.overwriteID);
          else
            new File(getPath(UUID.fromString(storeMsg.overwriteID))).delete(); // TODO handle if it doesn't work.

          File newBlock = new File(getPath(UUID.fromString(storeMsg.fileID)));
          newBlock.createNewFile();
          FileOutputStream output = new FileOutputStream(newBlock);

          output.write(storeMsg.contents.data);
          output.close();

          net.send(gson.toJson(new FileAcceptMessage(storeMsg.fileID)));
          break;
        case "uploadAccept":
          UploadAcceptMessage acceptMsg = gson.fromJson(msg, UploadAcceptMessage.class);
          UUID acceptedUUID = UUID.fromString(acceptMsg.uuid);
          printDebug(String.format("Block %s accepted.", acceptedUUID));
          storedFiles.add(acceptedUUID);
          if (onUploadAccept != null) {
            onUploadAccept.apply(acceptedUUID);
          }
          break;
        case "error":
          ErrorMessage errMsg = gson.fromJson(msg, ErrorMessage.class);
          printDebug(String.format("Error received from server: %s\n", errMsg.msg));
          if (errMsg.msg.equals("Banned for bad reliability.")) {
            banned = true;
            printDebug("Banned.");
          }
          break;
        default:
          printDebug(String.format("WARNING: Received unknown message type: %s.", type));

      }
    } catch (IOException e) {
      e.printStackTrace();
    } 
    return 0;
  }

  protected void printDebug(String s) {
    System.out.format("[ID %s]: %s\n", id, s);
  }

  public void uploadBlock(byte[] data, Function<UUID, Integer> onUploadAccept) throws IOException {
    this.onUploadAccept = onUploadAccept;
    net.send((new Gson()).toJson(new UploadMessage(data)));
  }

  public void downloadBlock(UUID id, Function<ByteBuffer, Integer> onDownload) throws IOException {
    this.onDownload = onDownload;
    printDebug(String.format("Requesting block %s from the server.", id));
    net.send((new Gson()).toJson(new DownloadMessage(id)));
  }

  private class AcceptWorkerMessage {
    private final String type = "acceptWorker";
    private final String workerID;

    public AcceptWorkerMessage(String id) {
      workerID = id;
    }
  }

  private class BufferJson {
    public String type = "Buffer";
    public byte[] data;

    public BufferJson(byte[] data) {
      this.data = data;
    }
  }

  private class DownloadMessage {
    public String type = "download";
    public String fileID;

    public DownloadMessage(UUID fileID) {
      this.fileID = fileID.toString();
    }
  }

  private class DownloadResponseMessage {
    public final String type = "downloadResponse";
    public BufferJson contents;
  }

  private class ErrorMessage {
    public String type = "error";
    public String msg;

    public ErrorMessage(String msg) {
      this.msg = msg;
    }
  }

  private class FileAcceptMessage {
    public String type = "fileAccept";
    public String fileID;

    public FileAcceptMessage(String fileID) {
      this.fileID = fileID;
    }
  }

  private class FileResponseMessage {
    public String type = "fileResponse";
    public String fileID;
    public BufferJson content;

    public FileResponseMessage(UUID fileID, byte[] content) {
      this.fileID = fileID.toString();
      this.content = new BufferJson(content);
    }
  }

  private class FileRequestMessage {
    public final String type = "fileRequest";
    public final String fileID;

    public FileRequestMessage(UUID blockID) {
      fileID = blockID.toString();
    }
  }

  private class JoinMessage {
    public String type = "join";
    public int donation = 0;
    public String[] uuids;

    public JoinMessage(int donation, Collection<UUID> uuids) {
      this.donation = donation;
      
      this.uuids = new String[uuids.size()];

      int i = 0;
      for (UUID uuid : uuids) {
        this.uuids[i] = uuid.toString();
        i++;
      }
    }
  }

  private class StoreFileMessage {
    public final String type = "storeFile";
    public final String fileID;
    public final String overwriteID;
    public final BufferJson contents;

    public StoreFileMessage(UUID ID, UUID overwriteID, byte[] contents) {
      this.fileID = ID.toString();
      this.overwriteID = overwriteID.toString();
      this.contents = new BufferJson(contents);
    }
  }

  private class UploadAcceptMessage {
    public String type = "uploadAccept";
    public final String uuid = null;
  }

  private class UploadMessage {
    public final String type = "upload";
    public BufferJson content;

    public UploadMessage(byte[] data) {
      content = new BufferJson(data);
    }
  }
}
