package networker;

import java.lang.StringBuilder;
import java.net.Socket;
import java.io.IOException;
import java.io.InputStream;
import java.util.function.Function;
import java.util.Arrays;
import java.nio.ByteBuffer;
import java.net.UnknownHostException;

public class Networker {

  private Socket sock; 
  private Function<String, Integer> onMessage;

  public Networker(Function<String, Integer> onMessage, String address, int port) throws UnknownHostException, IOException {
    this.onMessage = onMessage;  
    sock = new Socket(address, port);
  }

  public void send(String msg) throws IOException {
    byte[] ascii = msg.getBytes("US-ASCII");

    StringBuilder builder = new StringBuilder();
    builder.append(Integer.toString(ascii.length + 1)); // Plus one for the newline.
    builder.append("|");
    builder.append(msg);
    builder.append("\n");

    sock.getOutputStream().write(builder.toString().getBytes());
  }

  public void listen() throws IOException {
  
    InputStream input = sock.getInputStream();

    long messageLength = 0;
    int bytesRead = 0;
    StringBuilder builder = new StringBuilder();
    byte[] buffer = new byte[1024];

    while (!sock.isClosed()) {
      if (messageLength == 0) {
        ByteBuffer byteBuf = ByteBuffer.allocate(12);
        int strLength = 0;

        byte[] next;

        while (true) {
          next = input.readNBytes(1);
          if (next[0] == '|')
            break;
          byteBuf.put(next);
          strLength++;
        }

        // TODO catch NumberFormat exception
        messageLength = Long.parseLong(new String(Arrays.copyOfRange(byteBuf.array(), 0, strLength)));
      }

      int length = input.read(buffer);

      // Check if they disconnected.
      if (length == -1) {
        break;
      }

      builder.append(new String(buffer, 0, length, "US-ASCII"));
      bytesRead += length;

      if (bytesRead >= messageLength) {
        bytesRead = 0;
        messageLength = 0;

        String message = builder.toString();

        builder = new StringBuilder();
        
        onMessage.apply(message);
      }
    }
  }
}
