import pseudoworker.*;
import java.net.UnknownHostException;
import java.io.IOException;
import java.io.File;
import java.io.FileInputStream;
import org.apache.commons.cli.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.UUID;
import java.nio.ByteBuffer;

public class SimulatedCluster {
  private static final int BLOCK_SIZE = 1000000;
  private static final int REPLICATION_FACTOR = 2;
  private static Random rand = new Random();

  public static void main (String[] args) throws ParseException, UnknownHostException, IOException {
    new File("blocks").mkdir();

    Options clOptions = new Options();
    clOptions.addOption(new Option("d", false, "Run a defective worker alongside the normal workers."));
    clOptions.addOption(new Option("u", false, "Test upload functionality."));
    clOptions.addOption(new Option("c", true, "Choose number of workers to run (default 4)."));
    clOptions.addOption(new Option("h", true, "Specify hostname (default 127.0.0.1)."));
    clOptions.addOption(new Option("p", true, "Specify port (default 2113)."));
    clOptions.addOption(new Option("D", true, "Specify donation in MB (default 50)."));
    CommandLineParser parser = new DefaultParser();

    CommandLine parsedArgs = parser.parse(clOptions, args);

    int numWorkers = Integer.parseInt(parsedArgs.getOptionValue("c", "4"));
    String hostname = parsedArgs.getOptionValue("h", "127.0.0.1");
    int port = Integer.parseInt(parsedArgs.getOptionValue("p", "2113"));
    int donation = Integer.parseInt(parsedArgs.getOptionValue("D", "50"));
    ArrayList<PseudoWorker> workers = new ArrayList<PseudoWorker>();

    // Start defective worker if necessary
    if (parsedArgs.hasOption("d")) {
      numWorkers = 3;
      workers.add(new DefectiveWorker(hostname, port, donation));
    }

    // Start numWorkers pseudoworkers
    for (int i = 1; i <= numWorkers; i++) {
      workers.add(new PseudoWorker(hostname, port, donation));
    }

    // Give the workers some time to connect. TODO find a better way to do this.
    try {
      Thread.sleep(1000);
    } catch (java.lang.InterruptedException e) {
      e.printStackTrace();
    }

    // Test uploads
    if (parsedArgs.hasOption("u")) {
      System.out.println("Testing upload functionality.");
      testUpload(workers);
    }
  }

  private static void testUpload(ArrayList<PseudoWorker> workers) throws IOException {
    PseudoWorker uploader = workers.get(0);
    assert(!uploader.id.equals(""));

    // Generate random string of bytes.
    byte[] randBytes = new byte[BLOCK_SIZE];
    rand.nextBytes(randBytes);

    uploader.uploadBlock(randBytes, (UUID uuid) -> {
      try { 
        Thread.sleep(2000); // TODO find a better way
      } catch (java.lang.InterruptedException e) {
        e.printStackTrace();
      }

      try {
        uploader.downloadBlock(uuid, (ByteBuffer bytesList) -> {
          byte[] downloadedBytes = bytesList.array();

          if (Arrays.compare(randBytes, downloadedBytes) == 0)
            System.out.println("Upload test successful.");
          else
            System.out.println("Upload test failed. Downloaded block did not match uploaded block.");

          return 0;
        });
      } catch (IOException e) {
        e.printStackTrace();
      }

      return 0;
    });
  }
}
