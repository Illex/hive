# HIVE Virtual Cluster

Quickly create and connect multiple workers for testing or administration purposes.

# Building

`mvn clean compile assembly:single`

And then run the jar file which should be in the target directory. It will immediately connect a few
workers to the queen.

`java -ea -jar target/[jar-name-here]`

You will also want to remove the blocks directory each time the program is run. It isn't removed 
automatically for debugging purposes.

# Command Line Options

Use the flag `-d` to run one defective worker alongside the regular workers. Useful for testing.

Use the flag `-u` to run a simple test of upload functionality.

Use the option `-c [n]` to specify the number of workers to run. Default is 4.

Use the option `-h [hostname]` to specify the hostname. Default is 127.0.0.1.

Use the option `-p [port]` to specify the port. Default is 2113.

Use the option `-D [donation]` to specify donation per worker in Megabytes. Default is 50.
